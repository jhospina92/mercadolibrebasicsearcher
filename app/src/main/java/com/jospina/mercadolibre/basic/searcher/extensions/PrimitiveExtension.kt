package com.jospina.mercadolibre.basic.searcher.extensions

import java.text.DecimalFormat

fun Double?.formatAsCurrency(): String {
    if (this == null) {
        return ""
    }
    return DecimalFormat("$ ###,###").format(this)
}

fun Int?.formatValue(): String {
    if (this == null) {
        return ""
    }
    return DecimalFormat("###,###").format(this)
}