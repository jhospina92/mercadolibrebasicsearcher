package com.jospina.mercadolibre.basic.searcher.ui.activities.base

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.metrocuadrado.app.views.activities.base.BaseActivity

abstract class BaseActivityBinding<ViewModelType : ViewModel, ActivityBinding : ViewDataBinding>(val layout: Int) : BaseActivity<ViewModelType>(layout) {

    var binding: ActivityBinding? = null

    override fun setContentView() {
        binding = DataBindingUtil.setContentView(this, layout)
        binding?.lifecycleOwner = this
        bindViewModel()
    }

    fun refreshViewData() {
        binding?.invalidateAll()
    }

    abstract fun bindViewModel()
}