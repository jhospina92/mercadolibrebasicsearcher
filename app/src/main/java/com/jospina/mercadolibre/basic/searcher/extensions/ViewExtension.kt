package com.jospina.mercadolibre.basic.searcher.extensions

import android.content.Context
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso


fun View.show(condition: Boolean = true) {
    this.visibility = if (condition) View.VISIBLE else View.GONE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun ViewGroup.inflateLayout(layout: Int): View {
    return (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(layout, this, false)
}

fun View.isVisibleOnScreen(): Boolean {
    val scrollBounds = Rect()
    this.getHitRect(scrollBounds)
    return this.getLocalVisibleRect(scrollBounds) && visibility == View.VISIBLE && alpha > 0.1f
}

fun ImageView.loadFromUrl(url: String) {

    if (url.isEmpty()) {
        return
    }

    Picasso.get().load(url).into(this)
}