package com.jospina.mercadolibre.basic.searcher.ui.activities

import android.content.Context
import android.content.Intent
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.RecyclerView
import com.jospina.mercadolibre.basic.domain.entities.ResultError
import com.jospina.mercadolibre.basic.domain.entities.PaginationResult
import com.jospina.mercadolibre.basic.domain.entities.Product
import com.jospina.mercadolibre.basic.domain.entities.Site
import com.jospina.mercadolibre.basic.searcher.R
import com.jospina.mercadolibre.basic.searcher.dagger.providers.ApplicationComponentProvider
import com.jospina.mercadolibre.basic.searcher.extensions.formatValue
import com.jospina.mercadolibre.basic.searcher.extensions.hide
import com.jospina.mercadolibre.basic.searcher.extensions.show
import com.jospina.mercadolibre.basic.searcher.extensions.showToast
import com.jospina.mercadolibre.basic.searcher.ui.adapters.ProductsAdapter
import com.jospina.mercadolibre.basic.searcher.viewmodels.SearcherViewModel
import com.metrocuadrado.app.views.activities.base.BaseActivity
import kotlinx.android.synthetic.main.activity_searcher.recyclerView_activitySearcher_recyclerProducts as recyclerProducts
import kotlinx.android.synthetic.main.activity_searcher.textView_activitySearcher_textSiteName as textSiteName
import kotlinx.android.synthetic.main.activity_searcher.textView_activitySearcher_textPlaceholderResult as textPlaceholderResult
import kotlinx.android.synthetic.main.activity_searcher.textView_activitySearcher_textNotResults as textNotResults
import kotlinx.android.synthetic.main.activity_searcher.progressBar_activitySearcher_progressBar as progressBar
import kotlinx.android.synthetic.main.activity_searcher.textInputEditText_activitySearcher_inputSearch as inputSearch
import kotlinx.android.synthetic.main.activity_searcher.textView_activitySearcher_textCountResults as textCountResults

class SearcherActivity : BaseActivity<SearcherViewModel>(R.layout.activity_searcher) {

    private val productsAdapter = ProductsAdapter()

    override fun inject(applicationComponentProvider: ApplicationComponentProvider) {
        applicationComponentProvider.getSearcherComponent().inject(this)
    }

    override fun onIntentExtras() {
        super.onIntentExtras()

        if (!intent.hasExtra(EXTRA_SITE_ID)) {
            return onBackPressed()
        }

        viewModel.site = intent.extras?.getSerializable(EXTRA_SITE_ID) as Site
    }


    override fun onView() {
        super.onView()

        textSiteName.text = viewModel.site?.name
        recyclerProducts.adapter = productsAdapter

        showKeyboard()
    }


    override fun onListeners() {
        super.onListeners()

        inputSearch.setOnEditorActionListener { p, _, _ -> queryProductsAndClear();true }

        recyclerProducts.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (viewModel.lastPaginationResult.total - 1 <= getCountProductsInResult()) {
                    return
                }

                if (recyclerView.canScrollVertically(1) || viewModel.isRequestInProgress) {
                    return
                }

                productsAdapter.showLoader()
                recyclerView.scrollToPosition(getCountProductsInResult() + 1)
                viewModel.queryProducts(viewModel.lastPaginationResult.query)
            }
        })

        productsAdapter.setOnClickItemListener { startActivity(ProductActivity.buildIntent(this, it)) }
    }


    override fun onLiveData() {
        super.onLiveData()

        viewModel.paginationResultLiveData.observeSuccess(this) { onSuccessGetProducts(it) }
        viewModel.paginationResultLiveData.observeError(this) { onErrorGetProducts(it) }
    }


    private fun onSuccessGetProducts(paginationResult: PaginationResult<Product>) {

        viewModel.lastPaginationResult = paginationResult

        val totalCountResult = paginationResult.total
        textCountResults.text = resources.getQuantityString(R.plurals.results, totalCountResult, totalCountResult.formatValue())

        if (paginationResult.total == 0) {
            showNotResultData()
            return
        }

        renderItemsInResults(paginationResult)
    }

    private fun onErrorGetProducts(resultError: ResultError) {
        showToast(R.string.msg_error_default)
        showNotResultData()
    }

    private fun showProgressBar() {
        progressBar.show()
        recyclerProducts.hide()
        textPlaceholderResult.hide()
        textNotResults.hide()
    }

    private fun showNotResultData() {
        textNotResults.show()
        progressBar.hide()
        recyclerProducts.hide()
        textPlaceholderResult.hide()
    }

    private fun showResults() {
        recyclerProducts.show()
        progressBar.hide()
        textPlaceholderResult.hide()
        textNotResults.hide()
    }

    private fun queryProductsAndClear() {

        if (viewModel.isRequestInProgress) {
            return
        }

        recyclerProducts.scrollToPosition(0)
        recyclerProducts.scrollTo(0, 0)

        showProgressBar()
        dismissKeyboard()

        viewModel.lastPaginationResult = PaginationResult()
        viewModel.queryProducts(inputSearch.text.toString())
        productsAdapter.clear()
    }

    private fun renderItemsInResults(paginationResult: PaginationResult<Product>) {

        val items: MutableList<Any> = paginationResult.results.toMutableList()
        val itemCount = getCountProductsInResult()

        productsAdapter.removeLoader()

        if (itemCount == 0) {
            productsAdapter.loadItems(items)
        } else {
            productsAdapter.addItems(items)
            recyclerProducts.scrollToPosition(itemCount)
        }

        showResults()
    }

    private fun getCountProductsInResult(): Int {
        return productsAdapter.itemCount
    }

    private fun showKeyboard() {
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    private fun dismissKeyboard() {
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(inputSearch?.windowToken, 0)
    }

    companion object {

        private const val EXTRA_SITE_ID = "siteId"

        fun buildIntent(context: Context, site: Site): Intent {
            val intent = Intent(context, SearcherActivity::class.java)
            intent.putExtra(EXTRA_SITE_ID, site)
            return intent
        }
    }


}