package com.jospina.mercadolibre.basic.searcher.ui.activities

import android.content.Context
import android.content.Intent
import com.google.android.material.appbar.AppBarLayout
import com.jospina.mercadolibre.basic.domain.entities.Product
import com.jospina.mercadolibre.basic.searcher.R
import com.jospina.mercadolibre.basic.searcher.dagger.providers.ApplicationComponentProvider
import com.jospina.mercadolibre.basic.searcher.databinding.ActivityProductBinding
import com.jospina.mercadolibre.basic.searcher.extensions.showToast
import com.jospina.mercadolibre.basic.searcher.ui.activities.base.BaseActivityBinding
import com.jospina.mercadolibre.basic.searcher.viewmodels.ProductViewModel
import kotlinx.android.synthetic.main.activity_product.appBarLayout_activityProduct_appBar as appBarLayout
import kotlinx.android.synthetic.main.activity_product.textView_activityProduct_textToolbarTitle as textToolbarTitle
import kotlinx.android.synthetic.main.activity_product.toolbar_activityProduct_toolbar as toolbar


class ProductActivity() : BaseActivityBinding<ProductViewModel, ActivityProductBinding>(R.layout.activity_product) {

    override fun inject(applicationComponentProvider: ApplicationComponentProvider) {
        applicationComponentProvider.getProductComponent().inject(this)
    }

    override fun onIntentExtras() {
        super.onIntentExtras()

        if (!intent.hasExtra(EXTRA_PRODUCT)) {
            return onBackPressed()
        }

        viewModel.product = intent.extras?.getSerializable(EXTRA_PRODUCT) as Product
        viewModel.getProductById(viewModel.product?.id ?: "")
    }

    override fun bindViewModel() {
        binding?.viewModel = viewModel
    }

    override fun onListeners() {
        super.onListeners()

        appBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBar, offset -> appBarLayoutOffSetChangedListener(appBar, offset) })
    }

    override fun onLiveData() {
        super.onLiveData()

        viewModel.productLiveData.observeSuccess(this) {
            viewModel.product = it
            refreshViewData()
        }

        viewModel.productLiveData.observeError(this) {
            showToast(R.string.msg_error_default)
        }
    }


    /**
     * Makes a transition effect into title toolbar when scrolling down
     */
    private fun appBarLayoutOffSetChangedListener(appBarLayout: AppBarLayout, offset: Int) {
        val range = Math.abs(offset) - appBarLayout.totalScrollRange
        val alpha = 1.0F - (Math.abs(range).toFloat() / toolbar.height)

        textToolbarTitle.alpha = alpha
    }

    companion object {

        private const val EXTRA_PRODUCT = "product"

        fun buildIntent(context: Context, product: Product): Intent {
            val intent = Intent(context, ProductActivity::class.java)
            intent.putExtra(EXTRA_PRODUCT, product)
            return intent
        }
    }

}