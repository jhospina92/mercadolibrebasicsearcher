package com.jospina.mercadolibre.basic.searcher.ui.activities

import com.jospina.mercadolibre.basic.domain.entities.ResultError
import com.jospina.mercadolibre.basic.domain.entities.Site
import com.jospina.mercadolibre.basic.searcher.R
import com.jospina.mercadolibre.basic.searcher.dagger.providers.ApplicationComponentProvider
import com.jospina.mercadolibre.basic.searcher.extensions.showToast
import com.jospina.mercadolibre.basic.searcher.ui.adapters.SitesAdapter
import com.jospina.mercadolibre.basic.searcher.viewmodels.SiteSelectionViewModel
import com.metrocuadrado.app.views.activities.base.BaseActivity
import kotlinx.android.synthetic.main.activity_site_selection.recyclerView_activitySiteSelection_recyclerSites as recyclerSites

class SiteSelectionActivity : BaseActivity<SiteSelectionViewModel>(R.layout.activity_site_selection) {

    private val sitesAdapter = SitesAdapter()

    override fun inject(applicationComponentProvider: ApplicationComponentProvider) {
        applicationComponentProvider.getSiteSelectionComponent().inject(this)
    }

    override fun onView() {
        super.onView()

        recyclerSites.adapter = sitesAdapter
    }

    override fun onListeners() {
        super.onListeners()

        sitesAdapter.setOnClickItemListener { intentToSearcherActivity(it) }
    }

    override fun onLiveData() {
        super.onLiveData()

        viewModel.sites.observeSuccess(this) { onSuccessGetSites(it) }
        viewModel.sites.observeError(this) { onErrorGetSites(it) }
    }

    private fun onSuccessGetSites(sites: List<Site>) {
        sitesAdapter.loadItems(sites)
    }

    private fun onErrorGetSites(resultError: ResultError) {
        showToast(R.string.msg_error_default)
    }

    private fun intentToSearcherActivity(site: Site) {
        startActivity(SearcherActivity.buildIntent(this, site))
    }

}