package com.jospina.mercadolibre.basic.searcher.ui.adapters.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.jospina.mercadolibre.basic.searcher.R

class ItemSiteViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val buttonSite = view.findViewById<MaterialButton>(R.id.materialButton_itemSite_buttonSite)

    fun renderTextCountry(country: String) {
        buttonSite.text = country
    }

    fun setOnClickListener(listener: View.OnClickListener) {
        buttonSite.setOnClickListener(listener)
    }

}