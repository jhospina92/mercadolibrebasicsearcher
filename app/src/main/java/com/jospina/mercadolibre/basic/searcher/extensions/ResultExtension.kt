package com.jospina.mercadolibre.basic.searcher.extensions

import com.jospina.mercadolibre.basic.domain.handler.Result
import com.jospina.mercadolibre.basic.searcher.framework.lifecycle.ResponseLiveData

fun <T : Any> Result<T>.handleLiveData(liveData: ResponseLiveData<T>) {

    val callback = { it: Any -> liveData.postValue(it) }

    onSuccess(callback).onFailure(callback)
}