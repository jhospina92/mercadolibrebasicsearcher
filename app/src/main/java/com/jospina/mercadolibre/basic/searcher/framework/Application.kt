package com.jospina.mercadolibre.basic.searcher.framework

import android.app.Application
import com.jospina.mercadolibre.basic.domain.dagger.DomainModule
import com.jospina.mercadolibre.basic.searcher.BuildConfig
import com.jospina.mercadolibre.basic.searcher.dagger.components.*
import com.jospina.mercadolibre.basic.searcher.dagger.providers.ApplicationComponentProvider

class Application() : Application(), ApplicationComponentProvider {

    private val domainModule = DomainModule(BuildConfig.API_BASE_URL)
    var siteSelectionApplicationComponent: SiteSelectionApplicationComponent = DaggerSiteSelectionApplicationComponent.builder().domainModule(domainModule).build()
    var searcherApplicationComponent = DaggerSearcherApplicationComponent.builder().domainModule(domainModule).build()
    var productApplicationComponent = DaggerProductApplicationComponent.builder().domainModule(domainModule).build()

    override fun getSiteSelectionComponent(): SiteSelectionApplicationComponent {
        return siteSelectionApplicationComponent
    }

    override fun getSearcherComponent(): SearcherApplicationComponent {
        return searcherApplicationComponent
    }

    override fun getProductComponent(): ProductApplicationComponent {
        return productApplicationComponent
    }

}