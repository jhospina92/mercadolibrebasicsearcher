package com.jospina.mercadolibre.basic.searcher.viewmodels

import androidx.lifecycle.ViewModel
import com.jospina.mercadolibre.basic.domain.entities.Site
import com.jospina.mercadolibre.basic.domain.usecases.interfaces.IGetSitesUseCase
import com.jospina.mercadolibre.basic.searcher.extensions.handleLiveData
import com.jospina.mercadolibre.basic.searcher.framework.lifecycle.ResponseLiveData
import javax.inject.Inject

class SiteSelectionViewModel @Inject constructor(useCase: IGetSitesUseCase) : ViewModel() {

    val sites = ResponseLiveData<List<Site>>()

    init {
        useCase.invoke().handleLiveData(sites)
    }

}