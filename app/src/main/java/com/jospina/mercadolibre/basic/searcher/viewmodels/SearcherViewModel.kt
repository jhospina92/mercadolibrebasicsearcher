package com.jospina.mercadolibre.basic.searcher.viewmodels

import androidx.lifecycle.ViewModel
import com.jospina.mercadolibre.basic.domain.entities.PaginationResult
import com.jospina.mercadolibre.basic.domain.entities.Product
import com.jospina.mercadolibre.basic.domain.entities.Site
import com.jospina.mercadolibre.basic.domain.usecases.IQueryProductsUseCase
import com.jospina.mercadolibre.basic.searcher.extensions.handleLiveData
import com.jospina.mercadolibre.basic.searcher.framework.lifecycle.ResponseLiveData
import javax.inject.Inject

class SearcherViewModel @Inject constructor(private val useCase: IQueryProductsUseCase) : ViewModel() {

    val paginationResultLiveData = ResponseLiveData<PaginationResult<Product>>()

    var site: Site? = null
    var lastPaginationResult: PaginationResult<Product> = PaginationResult()
        //When lastPaginationResult var is set then means request is finished
        set(value) {
            isRequestInProgress = false
            field = value
        }

    var isRequestInProgress = false

    fun queryProducts(query: String) {

        val siteId = site?.id ?: return
        val offset = lastPaginationResult.offset + lastPaginationResult.limit

        isRequestInProgress = true

        useCase.invoke(siteId, query, offset).handleLiveData(paginationResultLiveData)
    }

}