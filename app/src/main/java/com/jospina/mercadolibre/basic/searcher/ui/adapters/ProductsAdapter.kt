package com.jospina.mercadolibre.basic.searcher.ui.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jospina.mercadolibre.basic.domain.entities.Product
import com.jospina.mercadolibre.basic.searcher.R
import com.jospina.mercadolibre.basic.searcher.extensions.inflateLayout
import com.jospina.mercadolibre.basic.searcher.ui.adapters.viewholders.ItemProductViewHolder
import com.jospina.mercadolibre.basic.searcher.ui.adapters.viewholders.ItemViewHolder

class ProductsAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items = mutableListOf<Any>()
    private var onClickItem: (Product) -> Unit = {}

    enum class ViewType(val id: Int) { ITEM(1), LOADER(2) }

    fun loadItems(items: List<Any>) {

        this.items.clear()
        this.items.addAll(items)

        notifyDataSetChanged()
    }

    fun addItems(items: List<Any>) {
        val positionStart = this.items.size
        this.items.addAll(items)
        notifyItemRangeInserted(positionStart, items.size)
    }

    override fun getItemViewType(position: Int): Int {

        val item = getItem(position)

        if (item is Product) {
            return ViewType.ITEM.id
        }

        if (item is ViewType) {
            return item.id
        }

        return ViewType.LOADER.id
    }


    fun showLoader() {

        if (this.items.contains(ViewType.LOADER))
            return

        this.items.add(ViewType.LOADER)
        notifyItemInserted(items.size)
    }

    fun removeLoader() {

        if (!this.items.contains(ViewType.LOADER))
            return

        this.items.removeAt(this.items.indexOf(ViewType.LOADER))
        notifyItemRemoved(this.items.size)
    }

    private fun getItem(position: Int): Any? {

        if (position < items.size) {
            return items[position]
        }

        return null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == ViewType.LOADER.id) {
            return ItemViewHolder(parent.inflateLayout(R.layout.item_loader))
        }

        return ItemProductViewHolder(parent.inflateLayout(R.layout.item_product))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = items.getOrNull(position)

        if (holder !is ItemProductViewHolder || item !is Product) {
            return
        }

        holder.renderProductImage(item.thumbnail)
        holder.renderProductTitle(item.title)
        holder.renderProductPrice(item.price, item.currency)
        holder.renderProductCondition(item.condition)
        holder.setOnClickListener(View.OnClickListener { onClickItem(item) })
    }

    fun setOnClickItemListener(callback: (Product) -> Unit) {
        this.onClickItem = callback
    }

    fun clear() {
        this.items.clear()
        notifyDataSetChanged()
    }

}