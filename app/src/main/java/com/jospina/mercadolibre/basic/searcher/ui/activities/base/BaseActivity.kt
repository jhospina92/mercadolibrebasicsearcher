package com.metrocuadrado.app.views.activities.base

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.jospina.mercadolibre.basic.searcher.dagger.providers.ApplicationComponentProvider
import javax.inject.Inject


@Suppress("UNCHECKED_CAST")
@SuppressLint("Registered")
abstract class BaseActivity<ViewModelType : ViewModel>(private val layout: Int) : AppCompatActivity() {

    @Inject
    protected lateinit var viewModel: ViewModelType

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        inject(application as ApplicationComponentProvider)

        setContentView()

        onIntentExtras()
        onView()
        onListeners()
        onLiveData()
    }

    protected open fun setContentView() {
        setContentView(layout)
    }

    protected open fun onIntentExtras() {}
    protected open fun onView() {}
    protected open fun onListeners() {}
    protected open fun onLiveData() {}

    abstract fun inject(applicationComponentProvider: ApplicationComponentProvider)
}