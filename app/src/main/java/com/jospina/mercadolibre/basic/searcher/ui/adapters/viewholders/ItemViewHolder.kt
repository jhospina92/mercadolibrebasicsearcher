package com.jospina.mercadolibre.basic.searcher.ui.adapters.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view)