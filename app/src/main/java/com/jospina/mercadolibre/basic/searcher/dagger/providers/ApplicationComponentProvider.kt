package com.jospina.mercadolibre.basic.searcher.dagger.providers

import com.jospina.mercadolibre.basic.searcher.dagger.components.ProductApplicationComponent
import com.jospina.mercadolibre.basic.searcher.dagger.components.SearcherApplicationComponent
import com.jospina.mercadolibre.basic.searcher.dagger.components.SiteSelectionApplicationComponent

interface ApplicationComponentProvider {

    fun getSiteSelectionComponent(): SiteSelectionApplicationComponent

    fun getSearcherComponent(): SearcherApplicationComponent

    fun getProductComponent(): ProductApplicationComponent

}