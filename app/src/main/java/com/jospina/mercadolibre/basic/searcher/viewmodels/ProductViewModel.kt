package com.jospina.mercadolibre.basic.searcher.viewmodels

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.ViewModel
import com.jospina.mercadolibre.basic.domain.entities.Product
import com.jospina.mercadolibre.basic.domain.usecases.interfaces.IGetProductByIdUseCase
import com.jospina.mercadolibre.basic.searcher.extensions.formatAsCurrency
import com.jospina.mercadolibre.basic.searcher.extensions.handleLiveData
import com.jospina.mercadolibre.basic.searcher.extensions.loadFromUrl
import com.jospina.mercadolibre.basic.searcher.framework.lifecycle.ResponseLiveData
import javax.inject.Inject


class ProductViewModel @Inject constructor(private val useCase: IGetProductByIdUseCase) : ViewModel() {


    val productLiveData = ResponseLiveData<Product>()
    var product: Product? = null


    fun getProductById(id: String) {
        useCase.invoke(id)?.handleLiveData(productLiveData)
    }

    fun getImage(): String {

        if (product?.pictures?.isNotEmpty() == true) {
            return product?.pictures?.first() ?: product?.thumbnail ?: ""
        }

        return product?.thumbnail ?: ""
    }

    fun getDisplayPrice(): String {
        return "${product?.price.formatAsCurrency()} ${product?.currency}"
    }

    companion object {

        @BindingAdapter("loadImage")
        @JvmStatic
        fun loadImage(view: ImageView, url: String) {
            view.loadFromUrl(url)
        }

    }

}