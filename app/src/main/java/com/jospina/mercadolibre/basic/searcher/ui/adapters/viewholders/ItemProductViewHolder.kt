package com.jospina.mercadolibre.basic.searcher.ui.adapters.viewholders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jospina.mercadolibre.basic.searcher.R
import com.jospina.mercadolibre.basic.searcher.extensions.formatAsCurrency
import com.jospina.mercadolibre.basic.searcher.extensions.loadFromUrl

class ItemProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val imageViewProductImage by lazy { view.findViewById<ImageView>(R.id.imageView_itemProduct_productImage) }
    private val textViewProductTitle by lazy { view.findViewById<TextView>(R.id.textView_itemProduct_productTitle) }
    private val textViewProductPrice by lazy { view.findViewById<TextView>(R.id.textView_itemProduct_productPrice) }
    private val textViewProductCondition by lazy { view.findViewById<TextView>(R.id.textView_itemProduct_productCondition) }

    fun renderProductImage(imageUrl: String) {
        imageViewProductImage.loadFromUrl(imageUrl)
    }

    fun renderProductTitle(title: String) {
        textViewProductTitle.text = title
    }

    fun renderProductPrice(value: Double, currency: String) {
        textViewProductPrice.text = "${value.formatAsCurrency()} $currency"
    }

    fun renderProductCondition(condition: String) {
        textViewProductCondition.text = condition
    }

    fun setOnClickListener(listener: View.OnClickListener) {
        itemView.setOnClickListener(listener)
    }
}
