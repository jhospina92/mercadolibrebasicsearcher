package com.jospina.mercadolibre.basic.searcher.ui.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jospina.mercadolibre.basic.domain.entities.Site
import com.jospina.mercadolibre.basic.searcher.R
import com.jospina.mercadolibre.basic.searcher.extensions.inflateLayout
import com.jospina.mercadolibre.basic.searcher.ui.adapters.viewholders.ItemSiteViewHolder

class SitesAdapter() : RecyclerView.Adapter<ItemSiteViewHolder>() {

    private var items = listOf<Site>()

    private var onClickItem: (Site) -> Unit = {}

    fun loadItems(items: List<Site>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemSiteViewHolder {
        return ItemSiteViewHolder(parent.inflateLayout(R.layout.item_site))
    }

    override fun getItemCount() = this.items.size

    override fun onBindViewHolder(holder: ItemSiteViewHolder, position: Int) {

        val item = items.getOrNull(position) ?: return

        holder.renderTextCountry(item.name)
        holder.setOnClickListener(View.OnClickListener { onClickItem(item) })
    }

    fun setOnClickItemListener(callback: (Site) -> Unit) {
        this.onClickItem = callback
    }


}