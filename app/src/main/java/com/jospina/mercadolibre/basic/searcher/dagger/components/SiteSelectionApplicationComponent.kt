package com.jospina.mercadolibre.basic.searcher.dagger.components

import com.jospina.mercadolibre.basic.domain.dagger.DomainModule
import com.jospina.mercadolibre.basic.searcher.ui.activities.SiteSelectionActivity
import com.jospina.mercadolibre.basic.searcher.viewmodels.SiteSelectionViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DomainModule::class])
interface SiteSelectionApplicationComponent {
    fun inject(activity: SiteSelectionActivity)
    fun viewModel(): SiteSelectionViewModel
}