package com.jospina.mercadolibre.basic.searcher.viewmodels.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jospina.mercadolibre.basic.domain.usecases.GetSitesUseCase
import com.jospina.mercadolibre.basic.searcher.viewmodels.SiteSelectionViewModel


class HomeViewModelFactory(val getSitesUseCase: GetSitesUseCase) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SiteSelectionViewModel::class.java)) {
            return SiteSelectionViewModel(getSitesUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
    }

}