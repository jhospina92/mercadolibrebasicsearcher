package com.jospina.mercadolibre.basic.searcher.framework.lifecycle

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.jospina.mercadolibre.basic.domain.entities.ResultError

class ResponseLiveData<T> : MutableLiveData<Any>() {

    fun observeError(owner: LifecycleOwner, callback: (ResultError) -> Unit): ResponseLiveData<T> {
        super.observe(owner, Observer { handleResponse({}, it as T, callback) })
        return this
    }

    fun observeSuccess(owner: LifecycleOwner, onResponse: (T) -> Unit): ResponseLiveData<T> {
        super.observe(owner, Observer { handleResponse(onResponse, it as T) })
        return this
    }

    private fun handleResponse(onResponseSuccess: (T) -> Unit, response: T?, onResponseError: (ResultError) -> Unit = {}) {

        if (response == null) {
            return
        }

        if (response is ResultError) {
            onResponseError(response)
            return
        }

        onResponseSuccess(response)
    }

}