package com.jospina.mercadolibre.basic.searcher.ui.activities

import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.filters.LargeTest
import com.jospina.mercadolibre.basic.domain.entities.PaginationResult
import com.jospina.mercadolibre.basic.domain.entities.Product
import com.jospina.mercadolibre.basic.domain.entities.ResultError
import com.jospina.mercadolibre.basic.domain.entities.Site
import com.jospina.mercadolibre.basic.domain.handler.Result
import com.jospina.mercadolibre.basic.domain.usecases.IQueryProductsUseCase
import com.jospina.mercadolibre.basic.searcher.R
import com.jospina.mercadolibre.basic.searcher.RandomGenerator
import com.jospina.mercadolibre.basic.searcher.dagger.components.DaggerTestSearcherApplicationComponent
import com.jospina.mercadolibre.basic.searcher.dagger.components.TestSearcherApplicationComponent
import com.jospina.mercadolibre.basic.searcher.dagger.modules.TestDomainModule
import com.kubo.vivamos.ui.activities.AndroidTestCase
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations
import javax.inject.Inject
import kotlin.test.assertFalse

@LargeTest
class SearcherActivityTest() : AndroidTestCase<SearcherActivity>(SearcherActivity::class.java) {

    @Inject
    lateinit var queryProductsUseCase: IQueryProductsUseCase
    private lateinit var appComponent: TestSearcherApplicationComponent

    private val resultExpected = Result<PaginationResult<Product>>()
    private val siteExpected = Site("MCO", "Colombia")

    private val queryText = RandomGenerator.generateString()
    private val paginationOffset = 0
    private val paginationLimit = 10

    private val inputSearch by lazy { findViewById<EditText>(R.id.textInputEditText_activitySearcher_inputSearch) }
    private val recyclerResult by lazy { findViewById<RecyclerView>(R.id.recyclerView_activitySearcher_recyclerProducts) }

    @Before
    fun setup() {

        MockitoAnnotations.initMocks(this)

        appComponent = DaggerTestSearcherApplicationComponent.builder().domainModule(TestDomainModule()).build()
        getApplication().searcherApplicationComponent = appComponent
        appComponent.inject(this)
    }

    private fun initActivity() {
        startActivity(SearcherActivity.buildIntent(getApplication().applicationContext, siteExpected))
    }

    @Test
    fun queryProductsHasResults() {

        whenever(queryProductsUseCase.invoke(siteExpected.id, queryText, paginationOffset)).thenReturn(resultExpected)

        initActivity()

        val productListExpected = generateProductList(RandomGenerator.getRandomInt(10, 20))
        val paginationResult = PaginationResult(queryText, productListExpected.size, paginationOffset, paginationLimit, productListExpected)

        assert(isShowingKeyboard(inputSearch))
        writeIntoInputEditTextAndPressActionButton(inputSearch.id, queryText)

        oneWait()

        assertFalse(isShowingKeyboard(inputSearch))
        validateViewIsShowing(R.id.progressBar_activitySearcher_progressBar)
        resultExpected.setSuccess(paginationResult)

        oneWait()
        Assert.assertEquals(paginationResult.results.size, recyclerResult.adapter?.itemCount ?: 0)
    }

    @Test
    fun validateLoadMoreResultWhenMakesScroll() {

        val limit = paginationLimit
        val totalCount = limit * 2

        whenever(queryProductsUseCase.invoke(siteExpected.id, queryText, paginationOffset)).thenReturn(resultExpected)
        whenever(queryProductsUseCase.invoke(siteExpected.id, queryText, limit)).thenReturn(resultExpected)

        initActivity()

        val paginationResultFirstLoad = PaginationResult(queryText, totalCount, paginationOffset, limit, generateProductList(limit))
        val paginationResultSecondLoad = PaginationResult(queryText, totalCount, limit, limit, generateProductList(limit))

        //Write input
        writeIntoInputEditTextAndPressActionButton(inputSearch.id, queryText)

        oneWait()

        validateViewIsShowing(R.id.progressBar_activitySearcher_progressBar)
        resultExpected.setSuccess(paginationResultFirstLoad)

        oneWait()
        Assert.assertEquals(paginationResultFirstLoad.results.size, recyclerResult.adapter?.itemCount ?: 0)

        swipeUpIntoView(recyclerResult.id)
        oneWait()

        resultExpected.setSuccess(paginationResultSecondLoad)

        oneWait()
        Assert.assertEquals(totalCount, recyclerResult.adapter?.itemCount ?: 0)
    }

    @Test
    fun validateNotResults() {

        whenever(queryProductsUseCase.invoke(siteExpected.id, queryText, paginationOffset)).thenReturn(resultExpected)

        initActivity()

        writeIntoInputEditTextAndPressActionButton(inputSearch.id, queryText)

        oneWait()
        resultExpected.setSuccess(PaginationResult())
        oneWait()
        validateViewIsShowing(R.id.textView_activitySearcher_textNotResults)
        validateViewIsHide(R.id.recyclerView_activitySearcher_recyclerProducts)
        validateViewIsHide(R.id.progressBar_activitySearcher_progressBar)
    }

    @Test
    fun validateClickInProduct() {

        val productListExpected = generateProductList(RandomGenerator.getRandomInt(10, 20))
        val paginationResult = PaginationResult(queryText, productListExpected.size, paginationOffset, paginationLimit, productListExpected)

        whenever(queryProductsUseCase.invoke(siteExpected.id, queryText, paginationOffset)).thenReturn(resultExpected)

        initActivity()

        writeIntoInputEditTextAndPressActionButton(inputSearch.id, queryText)

        oneWait()
        resultExpected.setSuccess(paginationResult)
        oneWait()

        val index = RandomGenerator.getRandomInt(0, 3)
        val titleExpected = productListExpected.getOrNull(index)?.title ?: ""

        getCurrentActivity().runOnUiThread { recyclerResult.getChildAt(index).callOnClick() }
        oneWait(300)
        assert(getCurrentActivity() is ProductActivity)
        Assert.assertEquals(titleExpected, findViewById<TextView>(R.id.textView_activityProduct_textTitle).text)
    }

    @Test
    fun validateQueryIsFailure() {

        whenever(queryProductsUseCase.invoke(siteExpected.id, queryText, paginationOffset)).thenReturn(resultExpected)

        initActivity()

        writeIntoInputEditTextAndPressActionButton(inputSearch.id, queryText)

        resultExpected.setError(ResultError())

        oneWait()
        validateViewIsShowing(R.id.textView_activitySearcher_textNotResults)
        validateViewIsHide(R.id.recyclerView_activitySearcher_recyclerProducts)
        validateViewIsHide(R.id.progressBar_activitySearcher_progressBar)

        validateShowingToast(R.string.msg_error_default)
    }


    private fun generateProductList(size: Int): List<Product> {

        val products = mutableListOf<Product>()

        for (i in 0 until size) {
            products.add(Product(RandomGenerator.generateString(), RandomGenerator.generateString()).apply {
                price = RandomGenerator.generateNumber(RandomGenerator.getRandomInt(1, 10)).toDouble()
            })
        }

        return products
    }


}