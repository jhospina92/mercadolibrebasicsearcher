package com.jospina.mercadolibre.basic.searcher.ui.activities

import android.widget.TextView
import androidx.test.filters.LargeTest
import com.jospina.mercadolibre.basic.domain.entities.Product
import com.jospina.mercadolibre.basic.domain.entities.ResultError
import com.jospina.mercadolibre.basic.domain.handler.Result
import com.jospina.mercadolibre.basic.domain.usecases.interfaces.IGetProductByIdUseCase
import com.jospina.mercadolibre.basic.searcher.R
import com.jospina.mercadolibre.basic.searcher.RandomGenerator
import com.jospina.mercadolibre.basic.searcher.dagger.components.DaggerTestProductApplicationComponent
import com.jospina.mercadolibre.basic.searcher.dagger.components.TestProductApplicationComponent
import com.jospina.mercadolibre.basic.searcher.dagger.modules.TestDomainModule
import com.jospina.mercadolibre.basic.searcher.extensions.formatAsCurrency
import com.kubo.vivamos.ui.activities.AndroidTestCase
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations
import javax.inject.Inject

@LargeTest
class ProductActivityTest() : AndroidTestCase<ProductActivity>(ProductActivity::class.java) {

    @Inject
    lateinit var getProductByIdUseCase: IGetProductByIdUseCase
    private lateinit var appComponent: TestProductApplicationComponent

    private val resultExpected = Result<Product>()

    val idExpected = RandomGenerator.generateString()
    val titleExpected = RandomGenerator.generateString()
    val productExpected = Product(idExpected, titleExpected).apply {
        price = RandomGenerator.generateNumber(RandomGenerator.getRandomInt(5, 10)).toDouble()
        currency = "COP"
    }


    @Before
    fun setup() {

        MockitoAnnotations.initMocks(this)

        appComponent = DaggerTestProductApplicationComponent.builder().domainModule(TestDomainModule()).build()
        getApplication().productApplicationComponent = appComponent
        appComponent.inject(this)

        whenever(getProductByIdUseCase.invoke(idExpected)).thenReturn(resultExpected)

        startActivity(ProductActivity.buildIntent(getApplication().applicationContext, productExpected))
    }

    @Test
    fun validateDisplayProduct() {

        productExpected.description = RandomGenerator.generateString(RandomGenerator.getRandomInt(100, 500))

        oneWait()
        resultExpected.setSuccess(productExpected)
        oneWait()

        Assert.assertEquals(titleExpected, findViewById<TextView>(R.id.textView_activityProduct_textTitle).text)
        Assert.assertEquals(productExpected.description, findViewById<TextView>(R.id.textView_activityProduct_textDescription).text)
        Assert.assertEquals("${productExpected.price.formatAsCurrency()} ${productExpected.currency}", findViewById<TextView>(R.id.textView_activityProduct_textPrice).text)
    }

    @Test
    fun validateFailure() {

        resultExpected.setError(ResultError())

        validateShowingToast(R.string.msg_error_default)
    }

}

