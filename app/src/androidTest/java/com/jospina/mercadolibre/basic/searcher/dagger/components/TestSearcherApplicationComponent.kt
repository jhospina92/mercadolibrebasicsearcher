package com.jospina.mercadolibre.basic.searcher.dagger.components

import com.jospina.mercadolibre.basic.domain.dagger.DomainModule
import com.jospina.mercadolibre.basic.searcher.ui.activities.SearcherActivityTest
import com.jospina.mercadolibre.basic.searcher.viewmodels.SearcherViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DomainModule::class])
interface TestSearcherApplicationComponent : SearcherApplicationComponent {
    fun inject(activity: SearcherActivityTest)
    override fun viewModel(): SearcherViewModel
}