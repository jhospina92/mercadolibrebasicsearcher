package com.kubo.vivamos.ui.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.ResultReceiver
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.intent.IntentStubberRegistry
import com.jospina.mercadolibre.basic.searcher.extensions.isVisibleOnScreen
import com.jospina.mercadolibre.basic.searcher.framework.Application
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.Rule
import org.mockito.Mockito
import java.io.Serializable
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.not


open class AndroidTestCase<T : Activity>(activityClass: Class<T>) {

    @get:Rule
    val testRule: ActivityTestRule<T> = ActivityTestRule(activityClass, false, false)

    private var currentActivity: AppCompatActivity? = null

    inline fun <reified T : Any> mock(): T = Mockito.mock(T::class.java)


    protected fun getApplication(): Application {
        return InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application
    }

    protected fun startActivity(intent: Intent? = null) {
        testRule.launchActivity(intent)
        monitorCurrentActivity(testRule)
    }

    protected fun getString(context: Context, text: Int): String {
        return context.getString(text)
    }

    protected fun getValue(context: Context, value: Int): Int {
        return context.resources.getInteger(value)
    }

    protected fun monitorCurrentActivity(activityTestRule: ActivityTestRule<*>) {

        currentActivity = activityTestRule.activity as AppCompatActivity

        if (IntentStubberRegistry.isLoaded()) {
            Intents.release()
        }

        Intents.init()

        activityTestRule.activity.application.registerActivityLifecycleCallbacks(object : android.app.Application.ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity?) {

            }

            override fun onActivityResumed(activity: Activity?) {
                if (activity is AppCompatActivity)
                    currentActivity = activity
            }

            override fun onActivityStarted(activity: Activity?) {
            }

            override fun onActivityDestroyed(activity: Activity?) {
            }

            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
            }

            override fun onActivityStopped(activity: Activity?) {
            }

            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
                if (activity is AppCompatActivity)
                    currentActivity = activity
            }

        })
    }

    protected fun intentToActivity(clazz: Class<*>, extras: MutableMap<String, Any> = mutableMapOf()) {

        val intent = Intent(currentActivity, clazz)

        for ((key, value) in extras) {

            if (value is String) {
                intent.putExtra(key, value)
                continue
            }

            if (value is Int) {
                intent.putExtra(key, value)
                continue
            }

            if (value is Class<*>) {
                intent.putExtra(key, value)
            }

            if (value is Serializable) {
                intent.putExtra("mission", value)
            }

        }

        currentActivity?.startActivity(intent)
    }

    protected fun getCurrentActivity(): AppCompatActivity {
        return currentActivity ?: throw Exception("Activity not found")
    }


    protected fun validateViewIsShowing(id: Int) {
        Assert.assertTrue(findViewById<View>(id).isVisibleOnScreen())
    }

    protected fun validateViewIsHide(id: Int) {
        Assert.assertFalse(findViewById<View>(id).isVisibleOnScreen())
    }

    protected fun <T : View> findViewById(id: Int): T {
        return currentActivity?.findViewById(id) ?: throw Exception("View not found");
    }

    protected fun writeIntoInputEditText(inputEditTextId: Int, text: String) {
        writeIntoInputEditText(0, inputEditTextId, text)
    }

    protected fun writeIntoInputEditText(inputLayoutId: Int, inputEditTextId: Int, text: String) {

        val textInputEditText = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(inputEditTextId),
                ViewMatchers.isDisplayed()
            )
        )
        textInputEditText.perform(ViewActions.click())

        val textInputEditText2 = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(inputEditTextId),
                ViewMatchers.isDisplayed()
            )
        )
        textInputEditText2.perform(ViewActions.replaceText(text))
    }

    protected fun writeIntoInputEditTextAndPressActionButton(inputLayoutId: Int, inputEditTextId: Int, text: String) {
        writeIntoInputEditText(inputLayoutId, inputEditTextId, text)
        pressActionButton(inputEditTextId)
    }

    protected fun writeIntoInputEditTextAndPressActionButton(inputEditTextId: Int, text: String) {
        writeIntoInputEditText(inputEditTextId, text)
        pressActionButton(inputEditTextId)
    }

    protected fun touchView(viewId: Int) {

        val view = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(viewId),
                ViewMatchers.isDisplayed()
            )
        )

        view.perform(ViewActions.click())
    }

    protected fun oneWait(delay: Long = 150) {
        Thread.sleep(delay)
    }

    protected fun swipeUpIntoView(id: Int) {
        Espresso.onView(ViewMatchers.withId(id)).perform(ViewActions.swipeUp())
    }

    protected fun swipeDownIntoView(id: Int) {
        Espresso.onView(ViewMatchers.withId(id)).perform(ViewActions.swipeDown())
    }

    protected fun swipeRightIntoView(id: Int) {
        Espresso.onView(ViewMatchers.withId(id)).perform(ViewActions.swipeLeft())
    }

    protected fun swipeLeftIntoView(id: Int) {
        Espresso.onView(ViewMatchers.withId(id)).perform(ViewActions.swipeRight())
    }

    protected fun isShowingKeyboard(view: View): Boolean {

        val imm = getCurrentActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val result = IMMResult()
        val res: Int

        imm.showSoftInput(view, 0, result)

        res = result.getResult()
        return (res == InputMethodManager.RESULT_UNCHANGED_SHOWN || res == InputMethodManager.RESULT_UNCHANGED_HIDDEN)
    }

    protected fun pressActionButton(viewId: Int) {
        Thread.sleep(1000)
        Espresso.onView(ViewMatchers.withId(viewId))
            .perform(ViewActions.pressImeActionButton())
    }

    protected fun validateShowingToast(message:Int){
        Espresso.onView(withText(message)).inRoot(withDecorView(not(getCurrentActivity().window.decorView))).check(matches(isDisplayed()))
    }
}

private class IMMResult : ResultReceiver(null) {

    private var result = -1

    override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
        result = resultCode
    }

    // poll result value for up to 500 milliseconds
    fun getResult(): Int {
        try {
            var sleep = 0
            while (result == -1 && sleep < 500) {
                Thread.sleep(100)
                sleep += 100
            }
        } catch (e: InterruptedException) {
        }

        return result
    }
}