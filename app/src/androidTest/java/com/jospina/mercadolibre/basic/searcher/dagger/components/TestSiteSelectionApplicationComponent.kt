package com.jospina.mercadolibre.basic.searcher.dagger.components

import com.jospina.mercadolibre.basic.domain.dagger.DomainModule
import com.jospina.mercadolibre.basic.searcher.ui.activities.SiteSelectionActivityTest
import com.jospina.mercadolibre.basic.searcher.dagger.components.SiteSelectionApplicationComponent
import com.jospina.mercadolibre.basic.searcher.viewmodels.SiteSelectionViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DomainModule::class])
interface TestSiteSelectionApplicationComponent : SiteSelectionApplicationComponent {
    fun inject(activity: SiteSelectionActivityTest)
    override fun viewModel(): SiteSelectionViewModel
}