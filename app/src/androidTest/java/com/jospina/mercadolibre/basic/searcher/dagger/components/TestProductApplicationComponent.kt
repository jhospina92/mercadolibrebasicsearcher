package com.jospina.mercadolibre.basic.searcher.dagger.components

import com.jospina.mercadolibre.basic.domain.dagger.DomainModule
import com.jospina.mercadolibre.basic.searcher.ui.activities.ProductActivityTest
import com.jospina.mercadolibre.basic.searcher.viewmodels.ProductViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DomainModule::class])
interface TestProductApplicationComponent : ProductApplicationComponent {
    fun inject(activity: ProductActivityTest)
    override fun viewModel(): ProductViewModel
}