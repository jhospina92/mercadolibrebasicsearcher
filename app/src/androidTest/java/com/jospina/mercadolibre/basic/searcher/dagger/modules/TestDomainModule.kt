package com.jospina.mercadolibre.basic.searcher.dagger.modules

import com.jospina.mercadolibre.basic.domain.dagger.DomainModule
import com.jospina.mercadolibre.basic.domain.usecases.IQueryProductsUseCase
import com.jospina.mercadolibre.basic.domain.usecases.interfaces.IGetProductByIdUseCase
import com.jospina.mercadolibre.basic.domain.usecases.interfaces.IGetSitesUseCase
import org.mockito.Mockito

class TestDomainModule : DomainModule("/") {

    override fun getSitesUseCase(): IGetSitesUseCase {
        return Mockito.mock(IGetSitesUseCase::class.java)
    }

    override fun queryProductsUseCase(): IQueryProductsUseCase {
        return Mockito.mock(IQueryProductsUseCase::class.java)
    }

    override fun getProductByIdUseCase(): IGetProductByIdUseCase {
        return Mockito.mock(IGetProductByIdUseCase::class.java)
    }
}