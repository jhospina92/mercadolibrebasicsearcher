package com.jospina.mercadolibre.basic.searcher

object RandomGenerator {

    fun generateString(length: Int = 16): String {
        val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9') + ' '
        return (1..length).map { kotlin.random.Random.nextInt(0, charPool.size) }.map(charPool::get).joinToString("")
    }

    fun generateNumber(length: Int = 10): Long {
        val numbersPool: List<Char> = ('1'..'9').toList()
        return (1..length).map { kotlin.random.Random.nextInt(0, numbersPool.size) }.map(numbersPool::get).joinToString("").toLong()
    }

    fun getRandomInt(min: Int, max: Int): Int {
        return (Math.random() * max + min).toInt()
    }

    fun generateFloat(min: Int, max: Int): Float {
        return (Math.random() * max + min).toFloat()
    }

}