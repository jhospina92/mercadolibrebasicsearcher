package com.jospina.mercadolibre.basic.searcher.ui.activities

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.test.filters.LargeTest
import com.jospina.mercadolibre.basic.domain.entities.ResultError
import com.jospina.mercadolibre.basic.domain.entities.Site
import com.jospina.mercadolibre.basic.domain.handler.Result
import com.jospina.mercadolibre.basic.domain.usecases.interfaces.IGetSitesUseCase
import com.jospina.mercadolibre.basic.searcher.R
import com.jospina.mercadolibre.basic.searcher.dagger.components.DaggerTestSiteSelectionApplicationComponent
import com.jospina.mercadolibre.basic.searcher.dagger.components.TestSiteSelectionApplicationComponent
import com.jospina.mercadolibre.basic.searcher.dagger.modules.TestDomainModule
import com.kubo.vivamos.ui.activities.AndroidTestCase
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations
import javax.inject.Inject

@LargeTest
class SiteSelectionActivityTest() : AndroidTestCase<SiteSelectionActivity>(SiteSelectionActivity::class.java) {

    @Inject
    lateinit var getSitesUseCase: IGetSitesUseCase
    private lateinit var appComponent: TestSiteSelectionApplicationComponent

    private val resultExpected = Result<List<Site>>()
    private val sitesExpected = listOf(Site("MCO", "Colombia"), Site("MLM", "Mexico"))

    @Before
    fun setup() {

        MockitoAnnotations.initMocks(this)

        appComponent = DaggerTestSiteSelectionApplicationComponent.builder().domainModule(TestDomainModule()).build()
        getApplication().siteSelectionApplicationComponent = appComponent
        appComponent.inject(this)

        whenever(getSitesUseCase.invoke()).thenReturn(resultExpected)
    }

    @Test
    fun validateSitesAreLoaded() {

        startActivity()

        resultExpected.setSuccess(sitesExpected)

        Thread.sleep(1)
        val recyclerSites = findViewById<RecyclerView>(R.id.recyclerView_activitySiteSelection_recyclerSites)
        Assert.assertEquals(sitesExpected.size, recyclerSites.adapter?.itemCount ?: 0)
    }

    @Test
    fun validateSitesIsFailure() {

        startActivity()

        resultExpected.setError(ResultError())

        validateShowingToast(R.string.msg_error_default)
    }

    @Test
    fun clickInSiteAndThenIntentToSearcherActivity() {

        startActivity()

        resultExpected.setSuccess(sitesExpected)

        Thread.sleep(1)

        val recyclerSites = findViewById<RecyclerView>(R.id.recyclerView_activitySiteSelection_recyclerSites)
        getCurrentActivity().runOnUiThread { (recyclerSites.getChildAt(0) as ViewGroup).getChildAt(0).callOnClick() }

        Thread.sleep(10)
        assert(getCurrentActivity() is SearcherActivity)
    }
}

