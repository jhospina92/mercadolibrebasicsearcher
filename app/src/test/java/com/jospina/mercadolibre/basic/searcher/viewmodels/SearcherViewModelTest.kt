package com.jospina.mercadolibre.basic.searcher.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jospina.mercadolibre.basic.domain.enums.ErrorType
import com.jospina.mercadolibre.basic.domain.handler.Result
import com.jospina.mercadolibre.basic.domain.entities.ResultError
import com.jospina.mercadolibre.basic.domain.entities.PaginationResult
import com.jospina.mercadolibre.basic.domain.entities.Product
import com.jospina.mercadolibre.basic.domain.entities.Site
import com.jospina.mercadolibre.basic.domain.usecases.QueryProductsUseCase
import com.jospina.mercadolibre.basic.searcher.TestCase
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class SearcherViewModelTest : TestCase() {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun validateQueryProducts() {

        val queryProductsUseCase = mock<QueryProductsUseCase>()
        val result = Result<PaginationResult<Product>>()
        val offsetExpected = 10
        val limitExpected = 30
        val offsetToSend = offsetExpected + limitExpected

        val products = listOf(Product(_e(), _e()))
        val paginationResultExpected = PaginationResult(_e(), 1000, offsetExpected, limitExpected, products)

        val site = Site("MCO", "Colombia")
        val query = "Celular"

        Mockito.`when`(queryProductsUseCase.invoke(site.id, query, offsetToSend)).thenReturn(result)

        val viewModel = SearcherViewModel(queryProductsUseCase)
        viewModel.lastPaginationResult = paginationResultExpected

        asyncTest { finish ->


            viewModel.site = site

            viewModel.paginationResultLiveData.observeSuccess(this) {

                viewModel.lastPaginationResult = it
                Assert.assertFalse(viewModel.isRequestInProgress)

                assert(it.results.isNotEmpty())

                //Validate pagination result
                Assert.assertEquals(paginationResultExpected.limit, it.limit)
                Assert.assertEquals(paginationResultExpected.offset, it.offset)
                Assert.assertEquals(paginationResultExpected.total, it.total)

                Mockito.verify(queryProductsUseCase).invoke(site.id, query, offsetToSend)

                finish()
            }

            viewModel.paginationResultLiveData.observeError(this, RESPONSE_NOT_EXPECTED)

            viewModel.queryProducts(query)

            Assert.assertTrue(viewModel.isRequestInProgress)
            result.setSuccess(paginationResultExpected)
        }
    }


    @Test
    fun validateQueryProductsFailure() {

        val queryProductsUseCase = mock<QueryProductsUseCase>()
        val result = Result<PaginationResult<Product>>()
        val errorExpected = ResultError(ErrorType.Network)
        val siteExpected= Site("MCO", "Colombia")

        Mockito.`when`(queryProductsUseCase.invoke(siteExpected.id, _e(), _e())).thenReturn(result)

        asyncTest { finish ->

            val viewModel = SearcherViewModel(queryProductsUseCase)
            viewModel.site = siteExpected

            viewModel.paginationResultLiveData.observeSuccess(this, RESPONSE_NOT_EXPECTED)

            viewModel.paginationResultLiveData.observeError(this) {
                Assert.assertEquals(errorExpected.type, it.type)
                finish()
            }

            viewModel.queryProducts(_e())
            result.setError(errorExpected)
        }
    }


}