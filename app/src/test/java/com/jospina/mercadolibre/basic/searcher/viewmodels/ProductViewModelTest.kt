package com.jospina.mercadolibre.basic.searcher.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jospina.mercadolibre.basic.domain.enums.ErrorType
import com.jospina.mercadolibre.basic.domain.handler.Result
import com.jospina.mercadolibre.basic.domain.entities.ResultError
import com.jospina.mercadolibre.basic.domain.entities.Product
import com.jospina.mercadolibre.basic.domain.usecases.GetProductByIdUseCase
import com.jospina.mercadolibre.basic.searcher.TestCase
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class ProductViewModelTest : TestCase() {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun validateGetProductByIdSuccess() {

        val getProductByIdUseCase = mock<GetProductByIdUseCase>()
        val result = Result<Product>()
        val productId = "MCO0292333"
        val productTitle = "productTitle"
        val productExpected = Product(productId, productTitle)

        Mockito.`when`(getProductByIdUseCase.invoke(productId)).thenReturn(result)

        asyncTest { finish ->

            val viewModel = ProductViewModel(getProductByIdUseCase)

            viewModel.productLiveData.observeSuccess(this) {

                Assert.assertEquals(productExpected.id, it.id)
                Assert.assertEquals(productExpected.title, it.title)

                Mockito.verify(getProductByIdUseCase).invoke(productId)

                finish()
            }

            viewModel.productLiveData.observeError(this, RESPONSE_NOT_EXPECTED)

            viewModel.getProductById(productId)
            result.setSuccess(productExpected)
        }
    }

    @Test
    fun validateGetProductByIdFailure() {

        val getProductByIdUseCase = mock<GetProductByIdUseCase>()
        val result = Result<Product>()
        val errorExpected = ResultError(ErrorType.Network)

        Mockito.`when`(getProductByIdUseCase.invoke(_e())).thenReturn(result)

        asyncTest { finish ->

            val viewModel = ProductViewModel(getProductByIdUseCase)

            viewModel.productLiveData.observeSuccess(this, RESPONSE_NOT_EXPECTED)

            viewModel.productLiveData.observeError(this) {
                Assert.assertEquals(errorExpected.type, it.type)
                finish()
            }

            viewModel.getProductById(_e())
            result.setError(errorExpected)
        }
    }

}