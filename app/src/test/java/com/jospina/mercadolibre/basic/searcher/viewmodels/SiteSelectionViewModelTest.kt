package com.jospina.mercadolibre.basic.searcher.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jospina.mercadolibre.basic.domain.enums.ErrorType
import com.jospina.mercadolibre.basic.domain.handler.Result
import com.jospina.mercadolibre.basic.domain.entities.ResultError
import com.jospina.mercadolibre.basic.domain.entities.Site
import com.jospina.mercadolibre.basic.domain.usecases.GetSitesUseCase
import com.jospina.mercadolibre.basic.searcher.TestCase
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class SiteSelectionViewModelTest : TestCase() {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun validateGetSitesSuccess() {

        val getSitesUseCase = mock<GetSitesUseCase>()
        val result = Result<List<Site>>()
        val listSitesExpected = listOf(Site("Id", "Name"))

        Mockito.`when`(getSitesUseCase.invoke()).thenReturn(result)

        asyncTest { finish ->

            val viewModel = SiteSelectionViewModel(getSitesUseCase)

            viewModel.sites.observeSuccess(this) {
                assert(it.isNotEmpty())
                finish()
            }

            viewModel.sites.observeError(this, RESPONSE_NOT_EXPECTED)

            result.setSuccess(listSitesExpected)
        }
    }

    @Test
    fun validateGetSitesFailure() {

        val getSitesUseCase = mock<GetSitesUseCase>()
        val result = Result<List<Site>>()
        val errorExpected = ResultError(ErrorType.Network)

        Mockito.`when`(getSitesUseCase.invoke()).thenReturn(result)

        asyncTest { finish ->

            val viewModel = SiteSelectionViewModel(getSitesUseCase)

            viewModel.sites.observeSuccess(this, RESPONSE_NOT_EXPECTED)
            
            viewModel.sites.observeError(this) {
                Assert.assertEquals(errorExpected.type, it.type)
                finish()
            }

            result.setError(errorExpected)
        }
    }

}