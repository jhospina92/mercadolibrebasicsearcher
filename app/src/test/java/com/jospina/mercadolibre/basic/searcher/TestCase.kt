package com.jospina.mercadolibre.basic.searcher

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import org.mockito.Mockito
import java.util.concurrent.CountDownLatch

abstract class TestCase : LifecycleOwner {

    private val lifecycle = LifecycleRegistry(this)

    init {
        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    override fun getLifecycle(): Lifecycle = lifecycle

    companion object {
        val RESPONSE_NOT_EXPECTED = { _: Any -> assert(false) }
    }

    fun asyncTest(callback: (countdown: () -> Unit) -> Unit, steps: Int = 1) {
        val signal = CountDownLatch(steps)
        callback { signal.countDown() }
        signal.await()
    }

    fun asyncTest(callback: (countdown: () -> Unit) -> Unit) {
        asyncTest(callback, 1)
    }

    //Empty value (Not matter)
    inline fun <reified T> _e(): T {

        val type: String = T::class.java.simpleName

        if (type.contains("String"))
            return "" as T

        if (type.contains("Integer"))
            return 0 as T

        if (type.contains("Long"))
            return 0L as T

        if (type.contains("Boolean"))
            return true as T


        return T::class.java.newInstance()
    }

    inline fun <reified T : Any> mock(): T = Mockito.mock(T::class.java)
}