
## Descripción del proyecto
--------------------
Este repositorio almacena una prueba técnica desarrollada en Android cuyo único propósito es demostrar las competencias técnicas necesarias de su autor para un proceso de reclutamiento en Mercado Libre. 

Sírvase de utilizarlo como recurso de estudio si considera que le sirve de provecho. :) 


## Configuración
--------------------

Clonar el repositorio, abrir el proyecto con Android Studio y ejecutar!

## Caracteristicas
--------------------

* Permite seleccionar cualquier pais en donde se encuentre Mercado Libre.
* Hay buscador de texto donde se puede buscar lo que sea y permite navegar entre los resultados en scroll infinito. 
* Visualiza la información basica asociada a un producto. 

## Arquitectura
--------------------
### Descripción de alto nivel

La aplicación fue desarrollada bajo una arquitectura en capa, desacomplando las responsabilidades en modulos que se comunican entre sí dependiendo del nivel de abstracción en la que se encuentra. Dada la solución para este proyecto tenemos: 

* **Capa de aplicación (app):** Este es el nivel más alto y es donde se encuentra todo el framework de Android con toda la interacción de las vistas y cuya arquitectura nivel de modulo es **Modelo-Vista-Modelo de vista (MVVM)**. Esta capa solo conoce e interactua con la capa de dominio que se encuentra en el segundo nivel. 
* **Capa de dominio:(domain)** Este es el nivel intermedio, es donde se encuentran definidas las reglas de negocio junto con sus casos de uso y se mantiene la consistencia del estado entre los objetos y modelos de datos. Esta capa solo conoce e interectaura con la capa de datos.
* **Capa de datos: (data):** Este es el nivel de más bajo de la arquitectura, es donde se encuentra la fuente de los datos y se encarga de administrarlos para servirlos a la capa de dominio. 

### Esquema visual de la arquitectura
![](https://i.ibb.co/1bpQFw5/arq-min.jpg)

### Especificaciones

* Todo el proyecto esta desarrollado 100% bajo el lenguaje de programación de **Kotlin**. 
* La capa de aplicación implementa la arquitectura de Modelo-Vista-Modelo de vista (MVVM). 
* La vista de producto es la unica que utiliza Databinding para vincular los datos de su *viewModel* a la vista de manera reactiva. Esto es asi solo para fines de este proyecto demostrar un enfoque diferente del la implementación de la arquitectura MVVM. 
* La aplicación es un cliente sin estado, es decir, todas las operaciones requieren una conexión de internet para poder realizar solicitudes a la API Rest de Mercadolibre.
* No cuenta con un sistema de persistencia de datos en local. 
* La entidades de dominio son utilizados tanto en la capa de dominio como en la capa de aplicación. 
* Los modelos en la capa de datos son traducidos  a la entidades de dominio y desconocen totalmente de su uso e implementación. 
* Mientras la aplicación se encuentre activa, todos los datos se almacenan en memoria. 
* Cada *Activity* son solo logica de presentación y cada una tiene su propio *viewModel* con sus propias dependencias inyectadas a través de **Dagger**. 

## Pantallazos
--------------------

![](https://i.ibb.co/gS43HnS/view-sites.jpg)![](https://i.ibb.co/2t56mNH/view-searcher.gif)![](https://i.ibb.co/SBK9kcT/view-product.gif)

## Dependencias externas

* [Retrofit:](https://square.github.io/retrofit/) Es la libreria de cliente REST que se utiliza en la capa de datos para poder gestionar la conexión con la API Rest de Mercadolibre. 
* [Picasso:](https://square.github.io/picasso/) Permite gestionar la carga de las imagenes en la capa de aplicación. 
* [Dagger:](https://developer.android.com/training/dependency-injection?hl=es-419) Permite simplicar la inyección de dependencias entre las capas. 
* [Mockito:](https://site.mockito.org/) Es un framework para testing que permite simplicar la realización de las pruebas unitarias. 

## License
```text
Copyright 2020 John Ospina

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```