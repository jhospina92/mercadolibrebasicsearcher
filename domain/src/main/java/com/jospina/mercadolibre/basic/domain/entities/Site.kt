package com.jospina.mercadolibre.basic.domain.entities

import java.io.Serializable

data class Site(val id: String, val name: String) : Serializable