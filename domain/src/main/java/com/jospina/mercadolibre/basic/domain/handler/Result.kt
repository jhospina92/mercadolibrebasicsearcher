package com.jospina.mercadolibre.basic.domain.handler

import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.domain.enums.ErrorType
import com.jospina.mercadolibre.basic.domain.entities.ResultError

class Result<T>() {

    var isSuccessful: Boolean = false
    var value: T? = null
    var resultError: ResultError? = null

    private var onSuccessCallback: (T) -> Unit = {}
    private var onErrorCallback: (ResultError) -> Unit = {}

    fun onSuccess(callback: (T) -> Unit): Result<T> {
        onSuccessCallback = callback
        return this
    }

    fun onFailure(callback: (ResultError) -> Unit): Result<T> {
        onErrorCallback = callback
        return this
    }

    fun setError(value: Any) {

        resultError = if (value is ErrorResponse) ResultError(ErrorType.Network, value.message) else if (value is ResultError) value else ResultError()

        onErrorCallback(resultError ?: ResultError())
    }

    fun setSuccess(value: T) {
        this.value = value
        this.isSuccessful = true
        onSuccessCallback(value)
    }

}