package com.jospina.mercadolibre.basic.domain.usecases

import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.domain.handler.Result

abstract class UseCase {

    protected fun <T> getCallbackFailure(result: Result<T>): (ErrorResponse) -> Unit {
        return { error: ErrorResponse -> result.setError(error) }
    }

}