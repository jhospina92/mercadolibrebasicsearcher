package com.jospina.mercadolibre.basic.domain.usecases.interfaces

import com.jospina.mercadolibre.basic.data.network.responses.ProductResponse
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductDescription
import com.jospina.mercadolibre.basic.data.repository.ItemRepository
import com.jospina.mercadolibre.basic.domain.factories.ProductFactory
import com.jospina.mercadolibre.basic.domain.handler.Result
import com.jospina.mercadolibre.basic.domain.entities.Product

interface IGetProductByIdUseCase {
    operator fun invoke(id: String): Result<Product>
}