package com.jospina.mercadolibre.basic.domain.usecases

import com.jospina.mercadolibre.basic.data.repository.SiteRepository
import com.jospina.mercadolibre.basic.domain.entities.Site
import com.jospina.mercadolibre.basic.domain.factories.SiteFactory
import com.jospina.mercadolibre.basic.domain.handler.Result
import com.jospina.mercadolibre.basic.domain.usecases.interfaces.IGetSitesUseCase


class GetSitesUseCase constructor(private val repository: SiteRepository) : UseCase(), IGetSitesUseCase {

    override operator fun invoke(): Result<List<Site>> {

        val result = Result<List<Site>>()

        val onSuccess = { data: List<com.jospina.mercadolibre.basic.data.network.responses.models.SiteModel> ->
            result.setSuccess(sortByNameDesc(data.map { SiteFactory.create(it) }))
        }

        repository.getSites(onSuccess, getCallbackFailure(result))

        return result
    }

    private fun sortByNameDesc(sites: List<Site>): List<Site> {
        return sites.sortedByDescending { it.name }.reversed()
    }

}