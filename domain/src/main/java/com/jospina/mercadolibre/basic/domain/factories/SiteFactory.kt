package com.jospina.mercadolibre.basic.domain.factories

import com.jospina.mercadolibre.basic.data.network.responses.models.SiteModel
import com.jospina.mercadolibre.basic.domain.entities.Site

object SiteFactory {
    fun create(siteModel: SiteModel) = Site(siteModel.id, siteModel.name)
}