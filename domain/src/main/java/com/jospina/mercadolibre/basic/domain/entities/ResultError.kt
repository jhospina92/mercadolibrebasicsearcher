package com.jospina.mercadolibre.basic.domain.entities

import com.jospina.mercadolibre.basic.domain.enums.ErrorType

data class ResultError(val type: ErrorType = ErrorType.Undefined, val message: String = "")