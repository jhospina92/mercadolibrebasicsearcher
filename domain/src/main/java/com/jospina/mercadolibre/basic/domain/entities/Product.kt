package com.jospina.mercadolibre.basic.domain.entities

import java.io.Serializable

data class Product(val id: String, val title: String) : Serializable {
    var thumbnail: String = ""
    var price: Double = 0.0
    var currency: String = ""
    var description: String = ""
    var condition: String = ""
    var pictures: List<String> = listOf()
}