package com.jospina.mercadolibre.basic.domain.factories

import com.jospina.mercadolibre.basic.data.network.responses.models.ProductResult
import com.jospina.mercadolibre.basic.domain.entities.Product

object ProductFactory {

    fun create(productResult: ProductResult) = Product(productResult.id, productResult.title).apply {
        thumbnail = productResult.thumbnail
        price = productResult.price
        currency = productResult.currencyId
        pictures = productResult.pictures.map { it.secureUrl }
        condition = productResult.getItemCondition()
    }

}