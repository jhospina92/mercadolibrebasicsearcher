package com.jospina.mercadolibre.basic.domain.usecases.interfaces

import com.jospina.mercadolibre.basic.domain.entities.Site
import com.jospina.mercadolibre.basic.domain.handler.Result

interface IGetSitesUseCase{
    operator fun invoke(): Result<List<Site>>
}