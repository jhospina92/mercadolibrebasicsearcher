package com.jospina.mercadolibre.basic.domain.usecases

import com.jospina.mercadolibre.basic.data.network.responses.QueryProductsResponse
import com.jospina.mercadolibre.basic.data.repository.SiteRepository
import com.jospina.mercadolibre.basic.domain.factories.ProductFactory
import com.jospina.mercadolibre.basic.domain.handler.Result
import com.jospina.mercadolibre.basic.domain.entities.PaginationResult
import com.jospina.mercadolibre.basic.domain.entities.Product

class QueryProductsUseCase(private val repository: SiteRepository) : UseCase(), IQueryProductsUseCase {

    override operator fun invoke(siteId: String, query: String, offset: Int): Result<PaginationResult<Product>> {

        val result = Result<PaginationResult<Product>>()

        val onSuccess = { response: QueryProductsResponse ->

            val productsMapper = response.results.map { ProductFactory.create(it) }
            val paginationResult = PaginationResult(response.query, response.pagination.total, response.pagination.offset, response.pagination.limit, productsMapper)

            result.setSuccess(paginationResult)
        }

        repository.queryProducts(siteId, query, offset, onSuccess, getCallbackFailure(result))

        return result
    }

}