package com.jospina.mercadolibre.basic.domain.usecases

import com.jospina.mercadolibre.basic.data.network.responses.ProductResponse
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductDescription
import com.jospina.mercadolibre.basic.data.repository.ItemRepository
import com.jospina.mercadolibre.basic.domain.factories.ProductFactory
import com.jospina.mercadolibre.basic.domain.handler.Result
import com.jospina.mercadolibre.basic.domain.entities.Product
import com.jospina.mercadolibre.basic.domain.usecases.interfaces.IGetProductByIdUseCase

class GetProductByIdUseCase constructor(private val repository: ItemRepository) : UseCase(), IGetProductByIdUseCase {

    override operator fun invoke(id: String): Result<Product> {
        val result = Result<Product>()

        val onSuccess = onSuccess@{ response: List<ProductResponse> ->

            if (response.isEmpty()) {
                result.setError(Error())
                return@onSuccess
            }

            val productResponse = response.first()
            val product = ProductFactory.create(productResponse.productResult)

            //has description?
            if (productResponse.productResult.descriptions.isEmpty()) {
                return@onSuccess result.setSuccess(product)
            }

            getDescription(result, product)
        }

        repository.getProducts(listOf(id), onSuccess, getCallbackFailure(result))

        return result
    }

    private fun getDescription(result: Result<Product>, product: Product) {

        val onSuccess = { it: ProductDescription ->
            product.description = it.plainText
            result.setSuccess(product)
        }

        repository.getProductDescription(product.id, onSuccess, getCallbackFailure(result))
    }

}