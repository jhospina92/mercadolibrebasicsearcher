package com.jospina.mercadolibre.basic.domain.usecases

import com.jospina.mercadolibre.basic.data.network.responses.QueryProductsResponse
import com.jospina.mercadolibre.basic.data.repository.SiteRepository
import com.jospina.mercadolibre.basic.domain.factories.ProductFactory
import com.jospina.mercadolibre.basic.domain.handler.Result
import com.jospina.mercadolibre.basic.domain.entities.PaginationResult
import com.jospina.mercadolibre.basic.domain.entities.Product

interface IQueryProductsUseCase {
    operator fun invoke(siteId: String, query: String, offset: Int): Result<PaginationResult<Product>>
}