package com.jospina.mercadolibre.basic.domain.dagger

import com.jospina.mercadolibre.basic.data.network.NetworkFactory
import com.jospina.mercadolibre.basic.data.network.api.SiteApi
import com.jospina.mercadolibre.basic.data.network.api.ItemApi
import com.jospina.mercadolibre.basic.data.network.services.SiteService
import com.jospina.mercadolibre.basic.data.network.services.ItemService
import com.jospina.mercadolibre.basic.data.repository.SiteRepository
import com.jospina.mercadolibre.basic.data.repository.ItemRepository
import com.jospina.mercadolibre.basic.domain.usecases.GetProductByIdUseCase
import com.jospina.mercadolibre.basic.domain.usecases.GetSitesUseCase
import com.jospina.mercadolibre.basic.domain.usecases.IQueryProductsUseCase
import com.jospina.mercadolibre.basic.domain.usecases.QueryProductsUseCase
import com.jospina.mercadolibre.basic.domain.usecases.interfaces.IGetProductByIdUseCase
import com.jospina.mercadolibre.basic.domain.usecases.interfaces.IGetSitesUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class DomainModule(private val apiBaseUrl: String) {

    @Provides
    @Singleton
    open fun getSitesUseCase(): IGetSitesUseCase {
        return GetSitesUseCase(SiteRepository.create(SiteService(NetworkFactory.createApi(SiteApi::class.java, apiBaseUrl))))
    }

    @Provides
    @Singleton
    open fun queryProductsUseCase(): IQueryProductsUseCase {
        return QueryProductsUseCase(SiteRepository.create(SiteService(NetworkFactory.createApi(SiteApi::class.java, apiBaseUrl))))
    }

    @Provides
    @Singleton
    open fun getProductByIdUseCase(): IGetProductByIdUseCase {
        return GetProductByIdUseCase(ItemRepository.create(ItemService(NetworkFactory.createApi(ItemApi::class.java, apiBaseUrl))))
    }

}