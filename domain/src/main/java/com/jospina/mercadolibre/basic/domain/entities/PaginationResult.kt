package com.jospina.mercadolibre.basic.domain.entities

data class PaginationResult<T>(val query: String = "", val total: Int = 0, val offset: Int = 0, val limit: Int = 0, val results: List<T> = listOf())