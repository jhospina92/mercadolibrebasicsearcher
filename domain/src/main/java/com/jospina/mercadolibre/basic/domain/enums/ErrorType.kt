package com.jospina.mercadolibre.basic.domain.enums

enum class ErrorType { Network, Undefined }