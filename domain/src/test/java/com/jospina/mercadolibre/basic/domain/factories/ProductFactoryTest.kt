package com.jospina.mercadolibre.basic.domain.factories

import com.jospina.mercadolibre.basic.data.network.responses.models.Picture
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductAttribute
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductResult
import org.junit.Assert
import org.junit.Test

class ProductFactoryTest {


    @Test
    fun createProduct() {

        val productIn = ProductResult().apply {
            id = "MCO-0001"
            title = "Product title"
            thumbnail = "http://mco-s1-p.mlstatic.com/645713-MCO32591675415_102019-I.jpg"
            price = 99999.0
            currencyId = "COP"
            pictures = listOf(Picture().apply { secureUrl = "https://mco-s1-p.mlstatic.com/603090-MCO32590879210_102019-O.jpg" })
            attributes = listOf(ProductAttribute().apply {
                condition = "ITEM_CONDITION"
                valueName = "Nuevo"
            })
        }


        val productOut = ProductFactory.create(productIn)

        Assert.assertEquals(productIn.id, productOut.id)
        Assert.assertEquals(productIn.title, productOut.title)
        Assert.assertEquals(productIn.thumbnail, productOut.thumbnail)
        assert(productIn.price == productOut.price)
        Assert.assertEquals(productIn.currencyId, productOut.currency)
        Assert.assertEquals(productIn.pictures.map { it.secureUrl }, productOut.pictures)
        Assert.assertEquals(productIn.getItemCondition(), productOut.condition)
    }

}