package com.jospina.mercadolibre.basic.domain.factories

import com.jospina.mercadolibre.basic.data.network.responses.models.SiteModel
import org.junit.Assert
import org.junit.Test

class SiteFactoryTest {

    @Test
    fun createSite() {

        val siteIn = SiteModel().apply {
            id = "MCO"
            name = "Colombia"
        }

        val siteOut = SiteFactory.create(siteIn)

        Assert.assertEquals(siteIn.id, siteOut.id)
        Assert.assertEquals(siteIn.name, siteOut.name)
    }

}