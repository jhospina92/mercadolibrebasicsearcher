package com.jospina.mercadolibre.basic.domain.usecases

import com.jospina.mercadolibre.basic.domain.JsonSampleResponse
import com.jospina.mercadolibre.basic.data.network.NetworkFactory
import com.jospina.mercadolibre.basic.data.network.api.ItemApi
import com.jospina.mercadolibre.basic.data.network.responses.enums.HttpCode
import com.jospina.mercadolibre.basic.data.network.services.ItemService
import com.jospina.mercadolibre.basic.data.repository.ItemRepository
import com.jospina.mercadolibre.basic.domain.AsyncTestCase
import com.jospina.mercadolibre.basic.domain.enums.ErrorType
import okhttp3.*
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GetProductByIdUseCaseTest : AsyncTestCase() {

    private val server = MockWebServer()
    private lateinit var baseUrlServer: HttpUrl
    private lateinit var service: ItemService
    private lateinit var repository: ItemRepository
    private lateinit var useCase: GetProductByIdUseCase

    @Before
    fun setup() {
        server.start()
        baseUrlServer = server.url("/")
        service = ItemService(NetworkFactory.createApi(ItemApi::class.java, baseUrlServer.toString()))
        repository = ItemRepository.create(service)
        useCase = GetProductByIdUseCase(repository)
    }

    @Test
    fun validateGetProductByIdResultsIsSuccess() {

        val responseGetProducts = MockResponse().setBody(JsonSampleResponse.SUCCESS_GET_PRODUCTS.raw)
        val responseGetProductDescription = MockResponse().setBody(JsonSampleResponse.SUCCESS_GET_PRODUCT_DESCRIPTION.raw)

        server.enqueue(responseGetProducts)
        server.enqueue(responseGetProductDescription)

        asyncTest { finish ->

            val result = useCase.invoke(_e())

            result.onSuccess {

                assert(result.isSuccessful)
                assert(result.value != null)

                result.value?.apply {
                    assert(this.id.isNotEmpty())
                    assert(this.title.isNotEmpty())
                    assert(this.pictures.isNotEmpty())
                    assert(this.price > 0)
                    assert(this.description.isNotEmpty())
                }

                finish()
            }

            result.onFailure(RESPONSE_NOT_EXPECTED)
        }
    }

    @Test
    fun validateGetProductByIdIsFailure() {

        val response = MockResponse().setBody(JsonSampleResponse.ERROR_INTERNAL_SERVER.raw)
        response.setResponseCode(HttpCode.CODE_SERVER_ERROR_INTERNAL)
        server.enqueue(response)

        asyncTest { finish ->

            val result = useCase.invoke(_e())

            result.onSuccess(RESPONSE_NOT_EXPECTED)
            result.onFailure { error -> Assert.assertEquals(ErrorType.Network, error.type);finish() }
        }
    }
}