package com.jospina.mercadolibre.basic.domain

import java.util.concurrent.CountDownLatch

abstract class AsyncTestCase {

    companion object {
        val RESPONSE_NOT_EXPECTED = { _: Any -> throw Exception("Response not expected") }
    }

    protected fun asyncTest(callback: (countdown: () -> Unit) -> Unit, steps: Int = 1) {
        val signal = CountDownLatch(steps)
        callback { signal.countDown() }
        signal.await()
    }

    protected fun asyncTest(callback: (countdown: () -> Unit) -> Unit) {
        asyncTest(callback, 1)
    }


    inline fun <reified T> _e(): T {

        val type: String = T::class.java.simpleName

        if (type.contains("String"))
            return "" as T

        if (type.contains("Integer"))
            return 0 as T

        if (type.contains("Boolean"))
            return true as T

        if (type.contains("List"))
            return listOf<T>() as T


        return T::class.java.newInstance()
    }

}