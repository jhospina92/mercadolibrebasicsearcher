package com.jospina.mercadolibre.basic.domain.usecases

import com.jospina.mercadolibre.basic.domain.AsyncTestCase
import com.jospina.mercadolibre.basic.domain.JsonSampleResponse
import com.jospina.mercadolibre.basic.data.network.NetworkFactory
import com.jospina.mercadolibre.basic.data.network.api.SiteApi
import com.jospina.mercadolibre.basic.data.network.responses.enums.HttpCode
import com.jospina.mercadolibre.basic.data.network.services.SiteService
import com.jospina.mercadolibre.basic.data.repository.SiteRepository
import com.jospina.mercadolibre.basic.domain.enums.ErrorType
import okhttp3.*
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GetSitesUseCaseTest : AsyncTestCase() {

    private val server = MockWebServer()
    private lateinit var baseUrlServer: HttpUrl
    private lateinit var service: SiteService
    private lateinit var repository: SiteRepository
    private lateinit var useCase: GetSitesUseCase

    @Before
    fun setup() {
        server.start()
        baseUrlServer = server.url("/")
        service = SiteService(NetworkFactory.createApi(SiteApi::class.java, baseUrlServer.toString()))
        repository = SiteRepository.create(service)
        useCase = GetSitesUseCase(repository)
    }

    @Test
    fun validateGetSitesResultSuccess() {

        val response = MockResponse().setBody(JsonSampleResponse.SUCCESS_GET_SITES.raw)
        server.enqueue(response)

        asyncTest { finish ->

            val result = useCase()

            result.onSuccess { assert(it.isNotEmpty());finish() }
            result.onFailure(RESPONSE_NOT_EXPECTED)
        }
    }

    @Test
    fun validateGetSitesResultFailure() {

        val response = MockResponse().setBody(JsonSampleResponse.ERROR_INTERNAL_SERVER.raw)
        response.setResponseCode(HttpCode.CODE_SERVER_ERROR_INTERNAL)
        server.enqueue(response)

        asyncTest { finish ->

            val result = useCase()

            result.onSuccess(RESPONSE_NOT_EXPECTED)
            result.onFailure { error -> Assert.assertEquals(ErrorType.Network, error.type);finish() }
        }
    }

}