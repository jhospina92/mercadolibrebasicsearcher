package com.jospina.mercadolibre.basic.domain.handler

import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.domain.AsyncTestCase
import com.jospina.mercadolibre.basic.domain.enums.ErrorType
import org.junit.Assert
import org.junit.Test
import kotlin.test.assertFalse

class ResultTest : AsyncTestCase() {

    @Test
    fun validateResultSuccess() {

        val result = Result<Int>()
        val valueExpected = 10

        asyncTest { finish ->
            result.onSuccess { Assert.assertEquals(valueExpected, it);finish() }
            result.onFailure(RESPONSE_NOT_EXPECTED)

            result.setSuccess(valueExpected)
            assert(result.isSuccessful)
            Assert.assertEquals(valueExpected, result.value)
        }
    }

    @Test
    fun validateResultAnyError() {

        val result = Result<Int>()
        val valueExpected = "Error message"

        asyncTest { finish ->

            result.onSuccess(RESPONSE_NOT_EXPECTED)
            result.onFailure { Assert.assertEquals(ErrorType.Undefined, it.type);finish() }

            result.setError(valueExpected)

            assertFalse(result.isSuccessful)
            Assert.assertNull(result.value)
            Assert.assertEquals(ErrorType.Undefined, result.resultError?.type)
        }
    }

    @Test
    fun validateResultErrorResponse() {

        val result = Result<Int>()
        val valueExpected = ErrorResponse()
        valueExpected.message = "Error message"

        asyncTest { finish ->

            result.onSuccess(RESPONSE_NOT_EXPECTED)
            result.onFailure { Assert.assertEquals(valueExpected.message, it.message);finish() }

            result.setError(valueExpected)

            assertFalse(result.isSuccessful)
            Assert.assertNull(result.value)
            Assert.assertEquals(ErrorType.Network, result.resultError?.type)
            Assert.assertEquals(valueExpected.message, result.resultError?.message)
        }
    }


}