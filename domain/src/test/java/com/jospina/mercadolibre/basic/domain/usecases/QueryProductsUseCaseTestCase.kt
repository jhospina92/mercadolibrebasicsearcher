package com.jospina.mercadolibre.basic.domain.usecases

import com.jospina.mercadolibre.basic.domain.AsyncTestCase
import com.jospina.mercadolibre.basic.domain.JsonSampleResponse
import com.jospina.mercadolibre.basic.data.network.NetworkFactory
import com.jospina.mercadolibre.basic.data.network.api.SiteApi
import com.jospina.mercadolibre.basic.data.network.responses.enums.HttpCode
import com.jospina.mercadolibre.basic.data.network.services.SiteService
import com.jospina.mercadolibre.basic.data.repository.SiteRepository
import com.jospina.mercadolibre.basic.domain.enums.ErrorType
import okhttp3.HttpUrl
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class QueryProductsUseCaseTestCase : AsyncTestCase() {

    private val server = MockWebServer()
    private lateinit var baseUrlServer: HttpUrl
    private lateinit var service: SiteService
    private lateinit var repository: SiteRepository
    private lateinit var useCase: QueryProductsUseCase

    @Before
    fun setup() {
        server.start()
        baseUrlServer = server.url("/")
        service = SiteService(NetworkFactory.createApi(SiteApi::class.java, baseUrlServer.toString()))
        repository = SiteRepository.create(service)
        useCase = QueryProductsUseCase(repository)
    }

    @Test
    fun validateQueryProductsIsSuccess() {

        val response = MockResponse().setBody(JsonSampleResponse.SUCCESS_GET_QUERY_PRODUCTS.raw)
        val paginationTotalExpected = 2307
        val paginationOffsetExpected = 0
        val paginationLimit = 5

        server.enqueue(response)

        asyncTest { finish ->

            val queryExpected = "QueryString"
            val result = useCase.invoke(_e(), queryExpected, _e())

            result.onSuccess {

                assert(result.isSuccessful)
                assert(result.value != null)

                result.value?.apply {
                    Assert.assertEquals(paginationLimit, limit)
                    Assert.assertEquals(paginationTotalExpected, total)
                    Assert.assertEquals(paginationOffsetExpected, offset)
                    assert(it.results.size == it.limit)
                }

                finish()
            }

            result.onFailure(RESPONSE_NOT_EXPECTED)
        }
    }

    @Test
    fun validateQueryProductsFailure() {

        val response = MockResponse().setBody(JsonSampleResponse.ERROR_INTERNAL_SERVER.raw)
        response.setResponseCode(HttpCode.CODE_SERVER_ERROR_INTERNAL)
        server.enqueue(response)

        asyncTest { finish ->

            val result = useCase(_e(), _e(),_e())

            result.onSuccess(RESPONSE_NOT_EXPECTED)
            result.onFailure { error -> Assert.assertEquals(ErrorType.Network, error.type);finish() }
        }
    }

}