package com.jospina.mercadolibre.basic.data.repository

import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.data.network.responses.QueryProductsResponse
import com.jospina.mercadolibre.basic.data.network.responses.models.SiteModel
import com.jospina.mercadolibre.basic.data.network.services.interfaces.SiteServiceInterface

object SiteRepository : BaseRepository<SiteServiceInterface>() {

    fun getSites(onSuccess: (List<SiteModel>) -> Unit, onFailure: (ErrorResponse) -> Unit) {
        service?.requestGetSites(onSuccess, onFailure)
    }

    fun queryProducts(siteId: String, query: String, offset: Int, onSuccess: (QueryProductsResponse) -> Unit, onFailure: (ErrorResponse) -> Unit) {
        service?.requestQueryProductsBySite(siteId, query, offset, onSuccess, onFailure)
    }

}