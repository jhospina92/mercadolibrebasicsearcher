package com.jospina.mercadolibre.basic.data.network.responses.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Seller {

    @SerializedName("id")
    @Expose
    var id: Int = 0

    @SerializedName("permalink")
    @Expose
    var permalink: String = ""
        get() = field ?: ""

    @SerializedName("registration_date")
    @Expose
    var registrationDate: String = ""
        get() = field ?: ""

    @SerializedName("car_dealer")
    @Expose
    var carDealer: Boolean = false

    @SerializedName("real_estate_agency")
    @Expose
    var realEstateAgency: Boolean = false

    @SerializedName("tags")
    @Expose
    var tags: List<String> = listOf()

}