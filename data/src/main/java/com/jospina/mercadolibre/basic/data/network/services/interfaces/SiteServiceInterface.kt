package com.jospina.mercadolibre.basic.data.network.services.interfaces

import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.data.network.responses.QueryProductsResponse
import com.jospina.mercadolibre.basic.data.network.responses.models.SiteModel

interface SiteServiceInterface {
    fun requestGetSites(onSuccess: (List<SiteModel>) -> Unit, onFailure: (ErrorResponse) -> Unit)
    fun requestQueryProductsBySite(siteId: String, query: String, offset: Int, onSuccess: (QueryProductsResponse) -> Unit, onFailure: (ErrorResponse) -> Unit)
}