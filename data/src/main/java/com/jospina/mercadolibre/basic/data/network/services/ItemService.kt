package com.jospina.mercadolibre.basic.data.network.services

import com.jospina.mercadolibre.basic.data.network.api.ItemApi
import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.data.network.responses.ProductResponse
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductDescription
import com.jospina.mercadolibre.basic.data.network.services.interfaces.ItemServiceInterface

class ItemService(private val api: ItemApi) : BaseService(), ItemServiceInterface {

    override fun requestGetProducts(ids: List<String>, onSuccess: (List<ProductResponse>) -> Unit, onFailure: (ErrorResponse) -> Unit) {

        val attributes = listOf("id", "title", "currency_id", "pictures", "price", "thumbnail", "attributes", "descriptions")

        val call = api.getProducts(ids.joinToString(","), attributes.joinToString(","))
        callEnqueue(call, onSuccess, onFailure)
    }

    override fun requestGetProductDescription(id: String, onSuccess: (ProductDescription) -> Unit, onFailure: (ErrorResponse) -> Unit) {
        val call = api.getProductDescription(id)
        callEnqueue(call, onSuccess, onFailure)
    }

}