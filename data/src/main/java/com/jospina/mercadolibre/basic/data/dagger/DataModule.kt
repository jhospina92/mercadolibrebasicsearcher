package com.jospina.mercadolibre.basic.data.dagger

import com.jospina.mercadolibre.basic.data.network.NetworkFactory
import com.jospina.mercadolibre.basic.data.network.api.ItemApi
import com.jospina.mercadolibre.basic.data.network.api.SiteApi
import com.jospina.mercadolibre.basic.data.network.services.ItemService
import com.jospina.mercadolibre.basic.data.network.services.SiteService
import com.jospina.mercadolibre.basic.data.repository.SiteRepository
import com.jospina.mercadolibre.basic.data.repository.ItemRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun getSiteRepository(): SiteRepository {
        return SiteRepository.create(SiteService(NetworkFactory.createApi(SiteApi::class.java, "/")))
    }


    @Provides
    @Singleton
    fun getItemRepository(): ItemRepository {
        return ItemRepository.create(ItemService(NetworkFactory.createApi(ItemApi::class.java, "/")))
    }

}