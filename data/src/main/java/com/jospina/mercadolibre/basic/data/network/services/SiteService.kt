package com.jospina.mercadolibre.basic.data.network.services

import com.jospina.mercadolibre.basic.data.network.api.SiteApi
import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.data.network.responses.QueryProductsResponse
import com.jospina.mercadolibre.basic.data.network.responses.models.SiteModel
import com.jospina.mercadolibre.basic.data.network.services.interfaces.SiteServiceInterface

class SiteService(private val api: SiteApi) : BaseService(), SiteServiceInterface {

    override fun requestGetSites(onSuccess: (List<SiteModel>) -> Unit, onFailure: (ErrorResponse) -> Unit) {
        val call = api.getListSites()
        callEnqueue(call, onSuccess, onFailure)
    }

    override fun requestQueryProductsBySite(siteId: String, query: String, offset: Int, onSuccess: (QueryProductsResponse) -> Unit, onFailure: (ErrorResponse) -> Unit) {
        val call = api.getQueryProducts(siteId, query, offset)
        callEnqueue(call, onSuccess, onFailure)
    }

}