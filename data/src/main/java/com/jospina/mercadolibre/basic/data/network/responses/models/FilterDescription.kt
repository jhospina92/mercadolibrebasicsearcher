package com.jospina.mercadolibre.basic.data.network.responses.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class FilterDescription {
    @SerializedName("id")
    @Expose
    var id: String = ""

    @SerializedName("name")
    @Expose
    var name: String = ""

    @SerializedName("type")
    @Expose
    var type: String = ""

    @SerializedName("values")
    @Expose
    var values: List<FilterValue> = listOf()

}
