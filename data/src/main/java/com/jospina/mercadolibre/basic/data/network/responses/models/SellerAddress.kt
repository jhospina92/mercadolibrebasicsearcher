package com.jospina.mercadolibre.basic.data.network.responses.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class SellerAddress {
    @SerializedName("id")
    @Expose
    var id: String = ""

    @SerializedName("comment")
    @Expose
    var comment: String = ""

    @SerializedName("address_line")
    @Expose
    var addressLine: String = ""

    @SerializedName("zip_code")
    @Expose
    var zipCode: String = ""

    @SerializedName("country")
    @Expose
    var country: AttributeName = AttributeName()

    @SerializedName("state")
    @Expose
    var state: AttributeName = AttributeName()

    @SerializedName("city")
    @Expose
    var city: AttributeName = AttributeName()

    @SerializedName("latitude")
    @Expose
    var latitude: String = ""

    @SerializedName("longitude")
    @Expose
    var longitude: String = ""

}