package com.jospina.mercadolibre.basic.data.network.responses.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Installment {
    @SerializedName("quantity")
    @Expose
    var quantity: Int = 0

    @SerializedName("amount")
    @Expose
    var amount: Double = 0.0

    @SerializedName("rate")
    @Expose
    var rate: Float = 0f

    @SerializedName("currency_id")
    @Expose
    var currencyId: String = ""

}
