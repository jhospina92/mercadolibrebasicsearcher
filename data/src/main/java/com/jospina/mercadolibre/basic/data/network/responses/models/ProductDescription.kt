package com.jospina.mercadolibre.basic.data.network.responses.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class ProductDescription {
    @SerializedName("text")
    @Expose
    var text: String = ""

    @SerializedName("plain_text")
    @Expose
    var plainText: String = ""

    @SerializedName("last_updated")
    @Expose
    var lastUpdated: String = ""

    @SerializedName("date_created")
    @Expose
    var dateCreated: String = ""
}