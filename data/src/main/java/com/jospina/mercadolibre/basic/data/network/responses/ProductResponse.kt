package com.jospina.mercadolibre.basic.data.network.responses

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductResult
import com.jospina.mercadolibre.basic.data.network.responses.enums.HttpCode


class ProductResponse {
    @SerializedName("code")
    @Expose
    var code: HttpCode = HttpCode.CLIENT_ERROR_BAD_REQUEST

    @SerializedName("body")
    @Expose
    var productResult: ProductResult = ProductResult()
}