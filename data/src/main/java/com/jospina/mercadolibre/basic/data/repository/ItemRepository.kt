package com.jospina.mercadolibre.basic.data.repository

import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.data.network.responses.ProductResponse
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductDescription
import com.jospina.mercadolibre.basic.data.network.services.ItemService
import com.jospina.mercadolibre.basic.data.network.services.interfaces.ItemServiceInterface

object ItemRepository : BaseRepository<ItemServiceInterface>() {

    fun getProducts(ids: List<String>, onSuccess: (List<ProductResponse>) -> Unit, onFailure: (ErrorResponse) -> Unit) {
        service?.requestGetProducts(ids, onSuccess, onFailure)
    }

    fun getProductDescription(id: String, onSuccess: (ProductDescription) -> Unit, onFailure: (ErrorResponse) -> Unit) {
        service?.requestGetProductDescription(id, onSuccess, onFailure)
    }

}