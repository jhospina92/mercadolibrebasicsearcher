package com.jospina.mercadolibre.basic.data.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkFactory {

    fun <T> createApi(apiClass: Class<T>, baseUrl: String): T {

        var retrofitBuilder = Retrofit.Builder()
        val httpClient = OkHttpClient.Builder()
        val contentType = "application/json"
        val isDebug = true

        httpClient.addInterceptor { chain ->

            var request = chain.request().newBuilder()


            request = request.addHeader("Content-Type", contentType)
            request = request.addHeader("Accept", contentType)

            chain.proceed(request.build())
        }

        //Logging debugger
        if (isDebug) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(logging)
        }

        retrofitBuilder = retrofitBuilder.baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).client(httpClient.build())

        return retrofitBuilder.build().create(apiClass)
    }


}