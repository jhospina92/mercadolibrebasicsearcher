package com.jospina.mercadolibre.basic.data.network.responses.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class ProductAttribute {
    @SerializedName("value_name")
    @Expose
    var valueName: String = ""

    @SerializedName("values")
    @Expose
    var values: List<ItemAttributeValue> = listOf()

    @SerializedName("attribute_group_id")
    @Expose
    var attributeGroupId: String = ""

    @SerializedName("id")
    @Expose
    var id: String = ""

    @SerializedName("name")
    @Expose
    var name: String = ""

    @SerializedName("source")
    @Expose
    var source: Long = 0L

    @SerializedName("value_id")
    @Expose
    var valueId: String = ""

    @SerializedName("attribute_group_name")
    @Expose
    var attributeGroupName: String = ""

}
