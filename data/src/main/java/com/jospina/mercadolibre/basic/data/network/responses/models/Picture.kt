package com.jospina.mercadolibre.basic.data.network.responses.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Picture {
    @SerializedName("secure_url")
    @Expose
    var secureUrl: String = ""

    @SerializedName("size")
    @Expose
    var size: String = ""

    @SerializedName("max_size")
    @Expose
    var maxSize: String = ""

    @SerializedName("quality")
    @Expose
    var quality: String = ""

    @SerializedName("id")
    @Expose
    var id: String = ""

    @SerializedName("url")
    @Expose
    var url: String = ""
}