package com.jospina.mercadolibre.basic.data.network.api

import com.jospina.mercadolibre.basic.data.network.responses.ProductResponse
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductDescription
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ItemApi {

    @GET("/items")
    fun getProducts(@Query("ids") ids: String, @Query("attributes") attributes: String): Call<List<ProductResponse>>

    @GET("/items/{productId}/description")
    fun getProductDescription(@Path("productId") productId: String):Call<ProductDescription>

}