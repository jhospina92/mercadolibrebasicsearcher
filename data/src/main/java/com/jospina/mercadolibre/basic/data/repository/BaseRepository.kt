package com.jospina.mercadolibre.basic.data.repository

import com.jospina.mercadolibre.basic.data.network.services.BaseService

abstract class BaseRepository<T> {

    protected var service: T? = null

    fun <R : BaseRepository<T>> create(service: T): R {
        this.service = service
        return this as R
    }

}