package com.jospina.mercadolibre.basic.data.network.responses.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class ResourceId {
    @SerializedName("id")
    @Expose
    var id: String = ""
}