package com.jospina.mercadolibre.basic.data.network.responses.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Shipping {
    @SerializedName("free_shipping")
    @Expose
    var freeShipping: Boolean = false

    @SerializedName("mode")
    @Expose
    var mode: String = ""

    @SerializedName("tags")
    @Expose
    var tags: List<String> = listOf()

    @SerializedName("logistic_type")
    @Expose
    var logisticType: String = ""

    @SerializedName("store_pick_up")
    @Expose
    var storePickUp: Boolean = false

}
