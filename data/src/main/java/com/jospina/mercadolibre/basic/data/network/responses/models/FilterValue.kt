package com.jospina.mercadolibre.basic.data.network.responses.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class FilterValue {
    @SerializedName("id")
    @Expose
    var id: String = ""

    @SerializedName("name")
    @Expose
    var name: String = ""

    @SerializedName("results")
    @Expose
    var results: Int = 0

    @SerializedName("path_from_root")
    @Expose
    var pathFromRoot: List<AttributeName> = listOf()

}
