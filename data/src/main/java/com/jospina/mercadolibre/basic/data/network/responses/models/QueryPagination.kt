package com.jospina.mercadolibre.basic.data.network.responses.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class QueryPagination {
    @SerializedName("total")
    @Expose
    var total: Int = 0

    @SerializedName("offset")
    @Expose
    var offset: Int = 0

    @SerializedName("limit")
    @Expose
    var limit: Int = 0

    @SerializedName("primary_results")
    @Expose
    var primaryResults: Int = 0

}
