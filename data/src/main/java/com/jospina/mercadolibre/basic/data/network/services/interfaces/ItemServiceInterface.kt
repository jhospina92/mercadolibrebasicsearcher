package com.jospina.mercadolibre.basic.data.network.services.interfaces

import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.data.network.responses.ProductResponse
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductDescription

interface ItemServiceInterface {
    fun requestGetProducts(ids: List<String>, onSuccess: (List<ProductResponse>) -> Unit, onFailure: (ErrorResponse) -> Unit)
    fun requestGetProductDescription(id: String, onSuccess: (ProductDescription) -> Unit, onFailure: (ErrorResponse) -> Unit)
}