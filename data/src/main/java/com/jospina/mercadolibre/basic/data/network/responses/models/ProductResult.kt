package com.jospina.mercadolibre.basic.data.network.responses.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class ProductResult {
    @SerializedName("id")
    @Expose
    var id: String = ""

    @SerializedName("site_id")
    @Expose
    var siteId: String = ""

    @SerializedName("title")
    @Expose
    var title: String = ""

    @SerializedName("seller")
    @Expose
    var seller: Seller = Seller()

    @SerializedName("price")
    @Expose
    var price: Double = 0.0

    @SerializedName("currency_id")
    @Expose
    var currencyId: String = ""
        get() = field ?: ""

    @SerializedName("available_quantity")
    @Expose
    var availableQuantity: Int = 0

    @SerializedName("sold_quantity")
    @Expose
    var soldQuantity: Int = 0

    @SerializedName("buying_mode")
    @Expose
    var buyingMode: String = ""

    @SerializedName("listing_type_id")
    @Expose
    var listingTypeId: String = ""

    @SerializedName("stop_time")
    @Expose
    var stopTime: String = ""

    @SerializedName("condition")
    @Expose
    var condition: String = ""

    @SerializedName("permalink")
    @Expose
    var permalink: String = ""

    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String = ""

    @SerializedName("accepts_mercadopago")
    @Expose
    var acceptsMercadopago: Boolean = false

    @SerializedName("installments")
    @Expose
    var installments: Installment = Installment()

    @SerializedName("address")
    @Expose
    var address: Address = Address()

    @SerializedName("shipping")
    @Expose
    var shipping: Shipping = Shipping()

    @SerializedName("seller_address")
    @Expose
    var sellerAddress: SellerAddress? = null

    @SerializedName("attributes")
    @Expose
    var attributes: List<ProductAttribute> = listOf()

    @SerializedName("original_price")
    @Expose
    var originalPrice: Double = 0.0
        get() = field ?: 0.0

    @SerializedName("category_id")
    @Expose
    var categoryId: String = ""

    @SerializedName("official_store_id")
    @Expose
    var officialStoreId: String = ""
        get() = field ?: ""

    @SerializedName("domain_id")
    @Expose
    var domainId: String = ""
        get() = field ?: ""

    @SerializedName("catalog_product_id")
    @Expose
    var catalogProductId: String = ""
        get() = field ?: ""

    @SerializedName("tags")
    @Expose
    var tags: List<String> = listOf()

    @SerializedName("pictures")
    @Expose
    var pictures: List<Picture> = listOf()

    @SerializedName("descriptions")
    @Expose
    var descriptions: List<ResourceId> = listOf()


    fun getItemCondition(): String {

        attributes.forEach {
            if (it.id == "ITEM_CONDITION") {
                return it.valueName
            }
        }

        return ""
    }

}
