package com.jospina.mercadolibre.basic.data.network.responses

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.jospina.mercadolibre.basic.data.network.responses.models.AttributeName
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductResult
import com.jospina.mercadolibre.basic.data.network.responses.models.FilterDescription
import com.jospina.mercadolibre.basic.data.network.responses.models.QueryPagination


class QueryProductsResponse {
    @SerializedName("site_id")
    @Expose
    var siteId: String = ""

    @SerializedName("query")
    @Expose
    var query: String = ""

    @SerializedName("paging")
    @Expose
    var pagination: QueryPagination = QueryPagination()

    @SerializedName("results")
    @Expose
    var results: List<ProductResult> = listOf()

    @SerializedName("secondary_results")
    @Expose
    var secondaryResults: List<ProductResult> = listOf()

    @SerializedName("related_results")
    @Expose
    var relatedResults: List<ProductResult> = listOf()

    @SerializedName("sort")
    @Expose
    var sort: AttributeName = AttributeName()

    @SerializedName("available_sorts")
    @Expose
    var availableSorts: List<AttributeName>? = null

    @SerializedName("filters")
    @Expose
    var filters: List<FilterDescription> = listOf()

    @SerializedName("available_filters")
    @Expose
    var availableFilters: List<FilterDescription> = listOf()

}
