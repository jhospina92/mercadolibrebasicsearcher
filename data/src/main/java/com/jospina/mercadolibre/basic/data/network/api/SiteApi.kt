package com.jospina.mercadolibre.basic.data.network.api

import com.jospina.mercadolibre.basic.data.network.responses.QueryProductsResponse
import com.jospina.mercadolibre.basic.data.network.responses.models.SiteModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.*

interface SiteApi {

    @GET("sites")
    fun getListSites(): Call<ArrayList<SiteModel>>

    @GET("sites/{siteId}/search")
    fun getQueryProducts(@Path("siteId") siteId: String, @Query("q") query: String, @Query("offset") offset: Int): Call<QueryProductsResponse>

}