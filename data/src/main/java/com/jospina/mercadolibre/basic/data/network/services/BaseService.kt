package com.jospina.mercadolibre.basic.data.network.services

import com.google.gson.Gson
import com.google.gson.JsonParser
import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.data.network.responses.enums.HttpCode
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

abstract class BaseService {

    fun <T> callEnqueue(call: Call<T>, onSuccess: (T) -> Unit, onFailure: (ErrorResponse) -> Unit) {

        val errorResponse = ErrorResponse()

        call.enqueue(object : Callback<T> {

            override fun onFailure(call: Call<T>, t: Throwable) {
                onFailure(errorResponse)
            }

            override fun onResponse(call: Call<T>, response: Response<T>) {

                val bodyResponseSuccess = response.body()

                if (response.isSuccessful && bodyResponseSuccess != null) {
                    return onSuccess(bodyResponseSuccess)
                }

                errorResponse.status = if (response.code() == HttpCode.CODE_SUCCESS_OK) HttpCode.CLIENT_ERROR_BAD_REQUEST else HttpCode.getByCode(response.code())
                onFailure(errorResponse)
            }

        })
    }
}