package com.jospina.mercadolibre.basic.data.network.responses.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class Address {
    @SerializedName("state_id")
    @Expose
    var stateId: String = ""

    @SerializedName("state_name")
    @Expose
    var stateName: String = ""

    @SerializedName("city_id")
    @Expose
    var cityId: String = ""

    @SerializedName("city_name")
    @Expose
    var cityName: String = ""

}
