package com.jospina.mercadolibre.basic.data.network.responses

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.jospina.mercadolibre.basic.data.network.responses.enums.HttpCode

class ErrorResponse {

    @SerializedName("message")
    @Expose
    var message: String = ""

    @SerializedName("error")
    @Expose
    var error: String = ""

    @SerializedName("status")
    @Expose
    var status: HttpCode = HttpCode.CLIENT_ERROR_BAD_REQUEST

    @SerializedName("cause")
    @Expose
    var cause: List<String> = listOf()

}