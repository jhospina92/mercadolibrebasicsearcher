package com.jospina.mercadolibre.basic.data.repository

import com.google.gson.Gson
import com.jospina.mercadolibre.basic.data.AsyncTestCase
import com.jospina.mercadolibre.basic.data.JsonSampleResponse
import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.data.network.responses.ProductResponse
import com.jospina.mercadolibre.basic.data.network.responses.enums.HttpCode
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductDescription
import com.jospina.mercadolibre.basic.data.network.services.interfaces.ItemServiceInterface
import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito

class ItemRepositoryTest : AsyncTestCase() {

    private val service = Mockito.mock(ItemServiceInterface::class.java)
    private val repository: ItemRepository = ItemRepository.create(service)

    @Test
    fun getProductsByServiceIsSuccess() {

        val ids = listOf<String>()

        val onSuccess = { products: List<ProductResponse> ->
            assert(products.isNotEmpty())
        }

        val onFailure = RESPONSE_NOT_EXPECTED

        repository.getProducts(ids, onSuccess, onFailure)

        Mockito.verify(service).requestGetProducts(ids, onSuccess, onFailure)

        //Simulate response
        onSuccess(listOf(ProductResponse()))
    }


    @Test
    fun getProductsByServiceIsFailure() {

        val ids = listOf<String>()

        val onSuccess = RESPONSE_NOT_EXPECTED

        val onFailure = { error: ErrorResponse ->
            assert(error.status.isClientError())
            Assert.assertEquals(HttpCode.CLIENT_ERROR_BAD_REQUEST, error.status)
        }

        repository.getProducts(ids, onSuccess, onFailure)

        Mockito.verify(service).requestGetProducts(ids, onSuccess, onFailure)

        //Simulate response
        onFailure(ErrorResponse())
    }

    @Test
    fun getProductDescriptionByServiceIsSuccess() {

        val idProduct = "idProduct"
        val responseExpected = Gson().fromJson(JsonSampleResponse.SUCCESS_GET_PRODUCT_DESCRIPTION.raw, ProductDescription::class.java)

        val onSuccess = { productDescription: ProductDescription ->
            assert(productDescription.plainText.isNotEmpty())
            Assert.assertEquals(responseExpected.plainText, responseExpected.plainText)
            Assert.assertEquals(responseExpected.dateCreated, responseExpected.dateCreated)
            Assert.assertEquals(responseExpected.lastUpdated, responseExpected.lastUpdated)
            Assert.assertEquals(responseExpected.text, responseExpected.text)
        }

        val onFailure = RESPONSE_NOT_EXPECTED

        repository.getProductDescription(idProduct, onSuccess, onFailure)

        Mockito.verify(service).requestGetProductDescription(idProduct, onSuccess, onFailure)

        //Simulate response
        onSuccess(responseExpected)
    }


    @Test
    fun getProductDescriptionByServiceIsFailure() {

        val ids = listOf<String>()
        val idProduct = "idProduct"

        val onSuccess = RESPONSE_NOT_EXPECTED

        val onFailure = { error: ErrorResponse ->
            assert(error.status.isClientError())
            Assert.assertEquals(HttpCode.CLIENT_ERROR_BAD_REQUEST, error.status)
        }

        repository.getProductDescription(idProduct, onSuccess, onFailure)

        Mockito.verify(service).requestGetProductDescription(idProduct, onSuccess, onFailure)

        //Simulate response
        onFailure(ErrorResponse())
    }

}