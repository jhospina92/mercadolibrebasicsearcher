package com.jospina.mercadolibre.basic.data.repository

import com.google.gson.Gson
import com.jospina.mercadolibre.basic.data.AsyncTestCase
import com.jospina.mercadolibre.basic.data.JsonSampleResponse
import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.data.network.responses.ProductResponse
import com.jospina.mercadolibre.basic.data.network.responses.QueryProductsResponse
import com.jospina.mercadolibre.basic.data.network.responses.enums.HttpCode
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductDescription
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductResult
import com.jospina.mercadolibre.basic.data.network.responses.models.SiteModel
import com.jospina.mercadolibre.basic.data.network.services.interfaces.ItemServiceInterface
import com.jospina.mercadolibre.basic.data.network.services.interfaces.SiteServiceInterface
import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito

class SiteRepositoryTest : AsyncTestCase() {

    private val service = Mockito.mock(SiteServiceInterface::class.java)
    private val repository: SiteRepository = SiteRepository.create(service)

    @Test
    fun getSitesByServiceIsSuccess() {

        val onSuccess = { sites: List<SiteModel> ->
            assert(sites.isNotEmpty())
        }

        val onFailure = RESPONSE_NOT_EXPECTED

        repository.getSites(onSuccess, onFailure)

        Mockito.verify(service).requestGetSites(onSuccess, onFailure)

        //Simulate response
        onSuccess(listOf(SiteModel()))
    }


    @Test
    fun getSitesByServiceIsFailure() {

        val onSuccess = RESPONSE_NOT_EXPECTED

        val onFailure = { error: ErrorResponse ->
            assert(error.status.isClientError())
            Assert.assertEquals(HttpCode.CLIENT_ERROR_BAD_REQUEST, error.status)
        }

        repository.getSites(onSuccess, onFailure)

        Mockito.verify(service).requestGetSites(onSuccess, onFailure)

        //Simulate response
        onFailure(ErrorResponse())
    }


    @Test
    fun getQueryProductsByServiceIsSuccess() {

        val sitedId = "MCO"
        val query = "AnyProductQuery"
        val offset = 0

        val onSuccess = { response: QueryProductsResponse ->
            assert(response.results.isNotEmpty())
        }

        val onFailure = RESPONSE_NOT_EXPECTED

        repository.queryProducts(sitedId, query, offset, onSuccess, onFailure)

        Mockito.verify(service).requestQueryProductsBySite(sitedId, query, offset, onSuccess, onFailure)

        //Simulate response
        onSuccess(QueryProductsResponse().apply { results = listOf(ProductResult()) })
    }


    @Test
    fun getQueryProductsByServiceIsFailure() {

        val onSuccess = RESPONSE_NOT_EXPECTED

        val onFailure = { error: ErrorResponse ->
            assert(error.status.isClientError())
            Assert.assertEquals(HttpCode.CLIENT_ERROR_BAD_REQUEST, error.status)
        }

        repository.queryProducts(_e(), _e(), _e(), onSuccess, onFailure)

        Mockito.verify(service).requestQueryProductsBySite(_e(), _e(), _e(), onSuccess, onFailure)

        //Simulate response
        onFailure(ErrorResponse())
    }


}