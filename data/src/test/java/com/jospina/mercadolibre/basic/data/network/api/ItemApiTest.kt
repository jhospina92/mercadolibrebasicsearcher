package com.jospina.mercadolibre.basic.data.network.api

import com.jospina.mercadolibre.basic.data.network.NetworkFactory
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ItemApiTest : ApiTest() {

    private val server = MockWebServer()
    private lateinit var baseUrlServer: String

    @Before
    fun setup() {
        server.start()
        baseUrlServer = server.url("/").toString()
    }

    /**
     * Get products
     */

    @Test
    fun validateRequestGetProductsIsSuccess() {

        val idsExpected = "MCO292933"
        val attributesExpected = "id,title"

        server.enqueue(MockResponse().setBody("[]"))
        NetworkFactory.createApi(ItemApi::class.java, baseUrlServer).getProducts(idsExpected, attributesExpected).execute()


        val request = server.takeRequest()

        val idsSent = request.requestUrl.queryParameter("ids")
        val attributesSent = request.requestUrl.queryParameter("attributes")

        Assert.assertEquals(idsExpected, idsSent)
        Assert.assertEquals(attributesExpected, attributesSent)

        validateJSONRequest(request, "GET")
    }

    /**
     * Get product description
     */

    @Test
    fun validateRequestGetProductDescriptionsIsSuccess() {

        val idExpected = "MCO292933"

        server.enqueue(MockResponse().setBody("{}"))
        NetworkFactory.createApi(ItemApi::class.java, baseUrlServer).getProductDescription(idExpected).execute()


        val request = server.takeRequest()

        val idSent = request.requestUrl.pathSegments().get(1)

        Assert.assertEquals(idExpected, idSent)

        validateJSONRequest(request, "GET")
    }



}