package com.jospina.mercadolibre.basic.data.network.api

import okhttp3.mockwebserver.RecordedRequest
import org.junit.Assert

abstract class ApiTest {

    companion object {
        private const val HEADER_ACCEPT = "Accept"
        private const val HEADER_CONTENT_TYPE = "Content-Type"
        private const val VALUE_CONTENT_TYPE_EXPECTED = "application/json"
    }

    protected fun validateJSONRequest(request: RecordedRequest, method: String) {

        val headers = request.headers.toMultimap()

        Assert.assertTrue(headers.containsKey(HEADER_ACCEPT))
        Assert.assertTrue(headers.containsKey(HEADER_CONTENT_TYPE))

        Assert.assertEquals(VALUE_CONTENT_TYPE_EXPECTED, request.getHeader(HEADER_CONTENT_TYPE))
        Assert.assertEquals(request.method, method)
    }
}