package com.jospina.mercadolibre.basic.data.network.services

import com.jospina.mercadolibre.basic.data.AsyncTestCase
import com.jospina.mercadolibre.basic.data.JsonSampleResponse
import com.jospina.mercadolibre.basic.data.network.NetworkFactory
import com.jospina.mercadolibre.basic.data.network.api.ItemApi
import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.data.network.responses.ProductResponse
import com.jospina.mercadolibre.basic.data.network.responses.models.ProductDescription
import com.jospina.mercadolibre.basic.data.network.responses.enums.HttpCode
import okhttp3.HttpUrl
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ItemServiceTest : AsyncTestCase() {

    private val server = MockWebServer()
    private lateinit var baseUrlServer: HttpUrl
    private lateinit var service: ItemService

    @Before
    fun setup() {
        server.start()
        baseUrlServer = server.url("/")
        service = ItemService(NetworkFactory.createApi(ItemApi::class.java, baseUrlServer.toString()))
    }

    /**
     * Get Products
     */

    @Test
    fun requestGetProductsIsSuccess() {

        asyncTest { finish ->

            val response = MockResponse().setBody(JsonSampleResponse.SUCCESS_GET_PRODUCTS.raw)
            server.enqueue(response)

            val onSuccess = { products: List<ProductResponse> ->
                assert(products.isNotEmpty())

                products.forEach {
                    Assert.assertEquals(it.code, HttpCode.SUCCESS_OK)
                    assert(it.productResult.pictures.isNotEmpty())
                }

                finish()
            }

            service.requestGetProducts(_e(), onSuccess, RESPONSE_NOT_EXPECTED)
        }
    }

    @Test
    fun requestGetProductsIsFailureByInternalErrorServer() {

        val response = MockResponse().setBody(JsonSampleResponse.ERROR_INTERNAL_SERVER.raw)
        response.setResponseCode(HttpCode.CODE_SERVER_ERROR_INTERNAL)

        server.enqueue(response)

        asyncTest { finish ->

            val onFailure = { error: ErrorResponse ->
                assert(error.status.isServerError())
                Assert.assertEquals(HttpCode.SERVER_ERROR_INTERNAL, error.status)
                finish()
            }

            service.requestGetProducts(_e(), RESPONSE_NOT_EXPECTED, onFailure)
        }
    }


    /**
     * Get Product description
     */

    @Test
    fun requestGetProductDescriptionIsSuccess() {

        asyncTest { finish ->

            val response = MockResponse().setBody(JsonSampleResponse.SUCCESS_GET_PRODUCT_DESCRIPTION.raw)
            server.enqueue(response)

            val onSuccess = { productDescription: ProductDescription ->
                assert(productDescription.plainText.isNotEmpty())
                finish()
            }

            service.requestGetProductDescription(_e(), onSuccess, RESPONSE_NOT_EXPECTED)
        }
    }

    @Test
    fun requestGetProductDescriptionIsFailureByNotFound() {

        val response = MockResponse().setBody(JsonSampleResponse.ERROR_NOT_FOUND.raw)
        response.setResponseCode(HttpCode.CODE_CLIENT_ERROR_NOT_FOUND)

        server.enqueue(response)

        asyncTest { finish ->

            val onFailure = { error: ErrorResponse ->
                assert(error.status.isClientError())
                Assert.assertEquals(HttpCode.CLIENT_ERROR_NOT_FOUND, error.status)
                finish()
            }

            service.requestGetProductDescription(_e(), RESPONSE_NOT_EXPECTED, onFailure)
        }
    }


}