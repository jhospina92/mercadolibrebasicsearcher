package com.jospina.mercadolibre.basic.data.network.responses.enums

import com.google.gson.Gson
import org.junit.Assert
import org.junit.Test

class HttpCodeTest {

    @Test
    fun validateCodeErrorInternalServerErrorIsSuccess() {
        Assert.assertEquals(HttpCode.SERVER_ERROR_INTERNAL.code, HttpCode.CODE_SERVER_ERROR_INTERNAL)
    }

    @Test
    fun validateCodeErrorBadRequestIsSuccess() {
        Assert.assertEquals(HttpCode.CLIENT_ERROR_BAD_REQUEST.code, HttpCode.CODE_CLIENT_ERROR_BAD_REQUEST)
    }

    @Test
    fun validateCodeErrorIsInfoResponse() {
        assert(HttpCode.INFO_CONTINUE.isInfoResponse())
        assert(HttpCode.INFO_PROCESSING.isInfoResponse())
        assert(HttpCode.INFO_SWITCHING_PROTOCOL.isInfoResponse())
    }

    @Test
    fun validateCodeErrorIsClientError() {
        assert(HttpCode.CLIENT_ERROR_BAD_REQUEST.isClientError())
        assert(HttpCode.CLIENT_ERROR_UNAUTHORIZED.isClientError())
        assert(HttpCode.CLIENT_ERROR_PAYMENT_REQUIRED.isClientError())
        assert(HttpCode.CLIENT_ERROR_FORBIDDEN.isClientError())
        assert(HttpCode.CLIENT_ERROR_NOT_FOUND.isClientError())
        assert(HttpCode.CLIENT_ERROR_METHOD_ALLOWED.isClientError())
        assert(HttpCode.CLIENT_ERROR_NOT_ACCEPTABLE.isClientError())
        assert(HttpCode.CLIENT_ERROR_PROXY_AUTHENTICATION_REQUIRED.isClientError())
        assert(HttpCode.CLIENT_ERROR_REQUEST_TIMEOUT.isClientError())
        assert(HttpCode.CLIENT_ERROR_CONFLICT.isClientError())
        assert(HttpCode.CLIENT_ERROR_GONE.isClientError())
        assert(HttpCode.CLIENT_ERROR_LENGTH_REQUIRED.isClientError())
        assert(HttpCode.CLIENT_ERROR_PRECONDITION_FAILED.isClientError())
        assert(HttpCode.CLIENT_ERROR_PAYLOAD_TOO_LARGE.isClientError())
        assert(HttpCode.CLIENT_ERROR_URI_TOO_LONG.isClientError())
        assert(HttpCode.CLIENT_ERROR_UNSUPPORTED_MEDIA_TYPE.isClientError())
        assert(HttpCode.CLIENT_ERROR_REQUESTED_RANGE_NOT_SATISFIABLE.isClientError())
        assert(HttpCode.CLIENT_ERROR_EXPECTATION_FAILED.isClientError())
        assert(HttpCode.CLIENT_ERROR_IAM_A_TEAPOT.isClientError())
        assert(HttpCode.CLIENT_ERROR_MISDIRECTED_REQUEST.isClientError())
        assert(HttpCode.CLIENT_ERROR_UNPROCESSABLE_ENTITY.isClientError())
        assert(HttpCode.CLIENT_ERROR_LOCKED.isClientError())
        assert(HttpCode.CLIENT_ERROR_FAILED_DEPENDENCY.isClientError())
        assert(HttpCode.CLIENT_ERROR_UPGRADE_REQUIRED.isClientError())
        assert(HttpCode.CLIENT_ERROR_PRECONDITION_REQUIRED.isClientError())
        assert(HttpCode.CLIENT_ERROR_TOO_MANY_REQUESTS.isClientError())
        assert(HttpCode.CLIENT_ERROR_REQUEST_HEADER_FIELDS_TOO_LARGE.isClientError())
        assert(HttpCode.CLIENT_ERROR_UNAVAILABLE_FOR_LEGAL_REASONS.isClientError())
    }

    @Test
    fun validateCodeErrorIsServerError() {
        assert(HttpCode.SERVER_ERROR_INTERNAL.isServerError())
        assert(HttpCode.SERVER_ERROR_NOT_IMPLEMENTED.isServerError())
        assert(HttpCode.SERVER_ERROR_BAD_GATEWAY.isServerError())
        assert(HttpCode.SERVER_ERROR_SERVICE_UNAVAILABLE.isServerError())
        assert(HttpCode.SERVER_ERROR_GATEWAY_TIMEOUT.isServerError())
        assert(HttpCode.SERVER_ERROR_HTTP_VERSION_NOT_SUPPORTED.isServerError())
        assert(HttpCode.SERVER_ERROR_VARIANT_ALSO_NEGOTIATES.isServerError())
        assert(HttpCode.SERVER_ERROR_INSUFFICIENT_STORAGE.isServerError())
        assert(HttpCode.SERVER_ERROR_LOOP_DETECTED.isServerError())
        assert(HttpCode.SERVER_ERROR_NOT_EXTENDED.isServerError())
        assert(HttpCode.SERVER_ERROR_NETWORK_AUTHENTICATION_REQUIRED.isServerError())
    }

    @Test
    fun validateCodeIsRedirectResponse() {
        assert(HttpCode.REDIRECT_MOVED_PERMANENTLY.isRedirectResponse())
        assert(HttpCode.REDIRECT_FOUND.isRedirectResponse())
        assert(HttpCode.REDIRECT_SEE_OTHER.isRedirectResponse())
        assert(HttpCode.REDIRECT_NOT_MODIFIED.isRedirectResponse())
        assert(HttpCode.REDIRECT_USE_PROXY.isRedirectResponse())
        assert(HttpCode.REDIRECT_UNUSED.isRedirectResponse())
        assert(HttpCode.REDIRECT_TEMPORARY.isRedirectResponse())
        assert(HttpCode.REDIRECT_PERMANENT_REDIRECT.isRedirectResponse())
    }

    @Test
    fun validateCodeIsSuccessful() {
        assert(HttpCode.SUCCESS_OK.isSuccessful())
        assert(HttpCode.SUCCESS_CREATED.isSuccessful())
        assert(HttpCode.SUCCESS_ACCEPTED.isSuccessful())
        assert(HttpCode.SUCCESS_NON_AUTHORITATIVE_INFORMATION.isSuccessful())
        assert(HttpCode.SUCCESS_NO_CONTENT.isSuccessful())
        assert(HttpCode.SUCCESS_RESET_CONTENT.isSuccessful())
        assert(HttpCode.SUCCESS_PARTIAL_CONTENT.isSuccessful())
    }

    @Test
    fun validateCodeIsError() {

        for (code in HttpCode.values()) {
            if (code.name.contains("_ERROR_"))
                assert(code.isError())
        }
    }

    @Test
    fun validateMethodGetByCodeIsSuccess() {
        Assert.assertEquals(HttpCode.SUCCESS_OK, HttpCode.getByCode(HttpCode.CODE_SUCCESS_OK))
        Assert.assertEquals(HttpCode.CLIENT_ERROR_BAD_REQUEST, HttpCode.getByCode(HttpCode.CODE_CLIENT_ERROR_BAD_REQUEST))
        Assert.assertEquals(HttpCode.SERVER_ERROR_INTERNAL, HttpCode.getByCode(HttpCode.CODE_SERVER_ERROR_INTERNAL))
        Assert.assertEquals(HttpCode.REDIRECT_FOUND, HttpCode.getByCode(HttpCode.CODE_REDIRECT_FOUND))
        Assert.assertEquals(HttpCode.INFO_CONTINUE, HttpCode.getByCode(HttpCode.CODE_INFO_CONTINUE))
    }

    @Test(expected = IllegalArgumentException::class)
    fun validateMethodGetByCodeIsFailureException() {
        HttpCode.getByCode(1001)
    }

    @Test
    fun validateCodeErrorGsonConvertSerializeIsSuccess() {
        for (code in HttpCode.values()) {
            Assert.assertEquals(code, Gson().fromJson(code.code.toString(), HttpCode::class.java))
        }
    }

}