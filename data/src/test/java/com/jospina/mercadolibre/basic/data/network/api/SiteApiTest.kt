package com.jospina.mercadolibre.basic.data.network.api

import com.jospina.mercadolibre.basic.data.network.NetworkFactory
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SiteApiTest : ApiTest() {

    private val server = MockWebServer()
    private lateinit var baseUrlServer: String

    @Before
    fun setup() {
        server.start()
        baseUrlServer = server.url("/").toString()
    }

    /**
     * Get sites
     */

    @Test
    fun validateRequestGetSitesIsSuccess() {
        server.enqueue(MockResponse().setBody("[]"))
        NetworkFactory.createApi(SiteApi::class.java, baseUrlServer).getListSites().execute()
        validateJSONRequest(server.takeRequest(), "GET")
    }

    /**
     * Query products
     */

    @Test
    fun validateRequestGetQueryItemsIsSuccess() {

        val siteIdExpected = "MCO"
        val queryExpected = "queryString"
        val offsetExpected = 20

        server.enqueue(MockResponse().setBody("{}"))
        NetworkFactory.createApi(SiteApi::class.java, baseUrlServer).getQueryProducts(siteIdExpected, queryExpected, offsetExpected).execute()

        val request = server.takeRequest()

        val siteIdSent = request.requestUrl.pathSegments().getOrNull(1)
        val querySent = request.requestUrl.queryParameter("q")
        val offsetSent = request.requestUrl.queryParameter("offset")?.toInt()

        Assert.assertEquals(siteIdExpected, siteIdSent)
        Assert.assertEquals(queryExpected, querySent)
        Assert.assertEquals(offsetExpected, offsetSent)

        validateJSONRequest(request, "GET")
    }


}