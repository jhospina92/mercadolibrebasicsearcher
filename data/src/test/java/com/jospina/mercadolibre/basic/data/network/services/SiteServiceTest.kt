package com.jospina.mercadolibre.basic.data.network.services

import com.jospina.mercadolibre.basic.data.AsyncTestCase
import com.jospina.mercadolibre.basic.data.JsonSampleResponse
import com.jospina.mercadolibre.basic.data.network.NetworkFactory
import com.jospina.mercadolibre.basic.data.network.api.SiteApi
import com.jospina.mercadolibre.basic.data.network.responses.ErrorResponse
import com.jospina.mercadolibre.basic.data.network.responses.QueryProductsResponse
import com.jospina.mercadolibre.basic.data.network.responses.models.SiteModel
import com.jospina.mercadolibre.basic.data.network.responses.enums.HttpCode
import okhttp3.HttpUrl
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SiteServiceTest : AsyncTestCase() {

    private val server = MockWebServer()
    private lateinit var baseUrlServer: HttpUrl
    private lateinit var service: SiteService

    @Before
    fun setup() {
        server.start()
        baseUrlServer = server.url("/")
        service = SiteService(NetworkFactory.createApi(SiteApi::class.java, baseUrlServer.toString()))
    }

    /**
     * Get Sites service
     */

    @Test
    fun requestGetSitesIsSuccess() {

        asyncTest { finish ->

            val response = MockResponse().setBody(JsonSampleResponse.SUCCESS_GET_SITES.raw)
            server.enqueue(response)

            val onSuccess = { sites: List<SiteModel> ->
                assert(sites.isNotEmpty())
                finish()
            }

            service.requestGetSites(onSuccess, RESPONSE_NOT_EXPECTED)
        }
    }

    @Test
    fun requestGetSitesIsFailureByInternalErrorServer() {

        val response = MockResponse().setBody(JsonSampleResponse.ERROR_INTERNAL_SERVER.raw)
        response.setResponseCode(HttpCode.CODE_SERVER_ERROR_INTERNAL)

        server.enqueue(response)

        asyncTest { finish ->

            val onFailure = { error: ErrorResponse ->
                assert(error.status.isServerError())
                Assert.assertEquals(HttpCode.SERVER_ERROR_INTERNAL, error.status)
                finish()
            }

            service.requestGetSites(RESPONSE_NOT_EXPECTED, onFailure)
        }
    }


    /**
     * Get Query Products
     */

    @Test
    fun requestGetQueryProductsIsSuccess() {

        val sitedId = _e<String>()
        val query = _e<String>()

        asyncTest { finish ->

            val response = MockResponse().setBody(JsonSampleResponse.SUCCESS_GET_QUERY_PRODUCTS.raw)
            server.enqueue(response)

            val onSuccess = { response: QueryProductsResponse ->

                assert(response.results.isNotEmpty())

                //Validate products attributes
                response.results.forEach {
                    assert(it.id.isNotEmpty())
                    assert(it.price > 0)
                    assert(it.categoryId.isNotEmpty())
                    assert(it.availableQuantity > 0)
                    assert(it.attributes.isNotEmpty())
                }

                //Validate pagination
                response.pagination.apply {
                    assert(offset == 0)
                    assert(limit > 0)
                    assert(total > 0)
                    assert(primaryResults > 0)
                }

                //Validate sort
                response.sort.apply {
                    assert(id.isNotEmpty())
                    assert(name.isNotEmpty())
                }

                finish()
            }

            service.requestQueryProductsBySite(sitedId, query, _e(), onSuccess, RESPONSE_NOT_EXPECTED)
        }
    }

    @Test
    fun requestGetQueryProductsIsFailureByNotFound() {

        val response = MockResponse().setBody(JsonSampleResponse.ERROR_NOT_FOUND.raw)
        response.setResponseCode(HttpCode.CODE_CLIENT_ERROR_NOT_FOUND)

        server.enqueue(response)

        asyncTest { finish ->

            val onFailure = { error: ErrorResponse ->
                assert(error.status.isClientError())
                Assert.assertEquals(HttpCode.CLIENT_ERROR_NOT_FOUND, error.status)
                finish()
            }

            service.requestQueryProductsBySite(_e(), _e(), _e(), RESPONSE_NOT_EXPECTED, onFailure)
        }
    }


}